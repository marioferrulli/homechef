package com.group10.homechefserver.resources;

import com.group10.homechefserver.model.Chat;
import com.group10.homechefserver.model.ChatPreview;
import com.group10.homechefserver.model.Conversations;
import com.group10.homechefserver.model.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@CrossOrigin(origins = "https://localhost:4200")
@RequestMapping("/messages")
public class MessageController {

    @Autowired
    RestTemplate restTemplate;


/*
@PostMapping(path = "/addMessageAndGetConversation")
    public Chat addMessageAndReturnConversation(@RequestBody Message message){
        messageRepository.save(message);
        int user1 = message.getIdSender();
        int user2 = message.getIdReceiver();
        List<Message> messageList = messageRepository.getOrderedConversation(user1, user2);
        Chat chat = new Chat();
        chat.setMessageList(messageList);
        return chat;
    }
 */

    @PostMapping(path = "/addMessageAndGetConversation")
    public Chat addMessageAndGetConversation(@RequestBody Message message) throws JSONException {

        String postMessageUrl = "http://messaging-service/messages/addMessageAndGetConversation";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        JSONObject messageJsonObject = new JSONObject();
        messageJsonObject.put("idReceiver", message.getIdReceiver());
        messageJsonObject.put("idSender", message.getIdSender());
        messageJsonObject.put("msg_text", message.getMsg_text());
        messageJsonObject.put("msg_timestamp", message.getMsg_timestamp());

        HttpEntity<String> request = new HttpEntity<String>(messageJsonObject.toString(), headers);

        Chat returnedUpdatedChat = restTemplate.postForObject(postMessageUrl, request, Chat.class);

        return returnedUpdatedChat;

    }

    @GetMapping(path = "/getConversation")
    public Chat getConversation(@RequestParam("user1") int user1, @RequestParam("user2") int user2){

        String getConversationUrl = "http://messaging-service/messages/getOrderedConversation/?user1=" + user1 + "&user2=" + user2;

        Chat returnedChat = restTemplate.getForObject(getConversationUrl, Chat.class);

        return returnedChat;


    }

    @GetMapping(path = "/getAllConversations/{idUser}")
    public Conversations getAllConversations(@PathVariable("idUser") int idUser){


        String url = "http://messaging-service/messages/getAllConversations/" + idUser;
        Conversations returnedConversations = restTemplate.getForObject(url, Conversations.class);

        return returnedConversations;

    }


}
