package com.group10.homechefserver.resources;

import com.fasterxml.jackson.databind.JsonNode;
import com.group10.homechefserver.dao.UserRepository;
import com.group10.homechefserver.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class AuthenticationController {


    RestTemplate restTemplate = new RestTemplate();

    @Autowired
    UserRepository userRepository;


    @CrossOrigin(origins = "https://localhost:4200")
    @RequestMapping(value = "/login/facebook/{client}", method = RequestMethod.POST)
    public @ResponseBody
    Map<String, String> getAccessToken(@PathVariable("client") String client, @RequestBody Map<String, String> token) throws JSONException {

        Map<String,String> jsonResult = new HashMap<>();
        int idUserSpring = -1;

        //Facebook token e UserID da Login Facebook
        String userID = token.get("userId");
        String userToken = token.get("token");
        System.out.println("UserID: " + userID);
        System.out.println("Access token: " + userToken);


        //AppToken: I token d'accesso dell'app sono usati per eseguire richieste alle API di Facebook per conto di un'app, anziché di un utente.
        String appTokenUrl = "";
        if (client.compareToIgnoreCase("mobileApp") == 0)
            appTokenUrl = "https://graph.facebook.com/oauth/access_token?client_id=2165566783752895&client_secret=7bcd1fed73ae05e0af512ba67898110f&grant_type=client_credentials";
        else if (client.compareToIgnoreCase("webApp") == 0)
            appTokenUrl = "https://graph.facebook.com/oauth/access_token?client_id=453715758590837&client_secret=4ad83110de5a43188d13a2122d6cd19a&grant_type=client_credentials";

        JsonNode appTokenResp = null;
        appTokenResp = restTemplate.getForObject(appTokenUrl, JsonNode.class);
        String appToken = appTokenResp.findValue("access_token").toString();

        //AppToken
        appToken = appToken.replace("\"", "");
        System.out.println("App token: " + appToken);


        //Debug dell'access token
        JsonNode resp = null;
        try {
            resp = restTemplate.getForObject("https://graph.facebook.com/debug_token?input_token=" + userToken + "&access_token=" + appToken, JsonNode.class);
        } catch (HttpClientErrorException e) {
            e.printStackTrace();
            String responseBody = e.getResponseBodyAsString();
            String status = e.getStatusText();
            System.out.println(responseBody);
            System.out.println(status);
        }

        System.out.println(resp.toString());
        Boolean isValid = resp.path("data").findValue("is_valid").asBoolean();



        if (isValid) {
            System.out.println("valido");
            String emailUrl = "https://graph.facebook.com/me?fields=name,email&access_token=" + userToken;

            JsonNode infoJson = null;
            infoJson = restTemplate.getForObject(emailUrl, JsonNode.class);
            System.out.println(infoJson.findValue("name"));
            System.out.println(infoJson.findValue("email"));
            String email = infoJson.findValue("email").toString().replace("\"","");
            String name = infoJson.findValue("name").toString().replace("\"","");
            String id = infoJson.findValue("id").toString().replace("\"","");
            System.out.println("Email user: " + email);
            System.out.println("Name user: " + name);

            int idFound = checkUserAlreadyKnown(email);

            if (idFound == -1) {
                User authenticatedUser = new User();
                authenticatedUser.setEmail(email);
                String[] completeName = name.split(" ");
                String firstName = "";
                String secondName = "";
                if (completeName.length == 2) {
                    firstName = completeName[0];
                    secondName = completeName[1];
                } else {
                    firstName = completeName[0];
                    secondName = null;
                }

                authenticatedUser.setName(firstName);
                authenticatedUser.setSurname(secondName);
                authenticatedUser.setFacebookId(id);

                userRepository.save(authenticatedUser);
                idUserSpring = authenticatedUser.getIdUser();
            }
            else
                idUserSpring = idFound;


            String jwtToken = getJwtToken(email);
            jsonResult.put("jwtToken", jwtToken);
            jsonResult.put("idUserSpring", idUserSpring+"");
            return jsonResult;


        } else {
            System.out.println("non valido");
        }


        return jsonResult;
    }

    private int checkUserAlreadyKnown(String email) {
        List<User> retrievedUser = userRepository.findByEmail(email);
        if (!retrievedUser.isEmpty())
            return retrievedUser.get(0).getIdUser();
        else
            return -1;
    }



    private String getJwtToken(String email) throws JSONException {
        String authenticationUrl = "http://localhost:8080/authenticate";

        // create headers
        HttpHeaders headers = new HttpHeaders();
        // set `content-type` header
        headers.setContentType(MediaType.APPLICATION_JSON);
        // set `accept` header
        //headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        // request body parameters
        Map<String, Object> map = new HashMap<>();
        map.put("username", email);

        // build the request
        HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);

        // send POST request
        ResponseEntity<String> response = restTemplate.postForEntity(authenticationUrl, entity, String.class);

        System.out.println(response.getBody());
        String s1 = response.getBody().replace("{\"token\":\"", "");
        String cleanToken = s1.replace("\"}", "");
        return cleanToken;

    }


}
