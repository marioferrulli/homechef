package com.group10.homechefserver.resources;

import com.group10.homechefserver.dao.ReviewResponseRepository;
import com.group10.homechefserver.model.Review;
import com.group10.homechefserver.model.ReviewResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;

@RestController
@CrossOrigin(origins = "https://localhost:4200")
@RequestMapping("/responses")
public class ReviewResponseResource {

    @Autowired
    ReviewResponseRepository reviewResponseRepository;

    @GetMapping(path="/findResponsesByReviewAndUser/")
    public Iterable<ReviewResponse> getReceivedReviews(@RequestParam("idReview") int idReview, @RequestParam("idUser") int idUser) {
        return reviewResponseRepository.findResponsesByReviewAndUser(idReview, idUser);
    }

    @PostMapping(path="/addReviewResponse/") // Map ONLY POST Requests
    public String addNewReview (@RequestBody ReviewResponse rr) {
        reviewResponseRepository.save(rr);
        String idReviewResponse = rr.getIdResponse() + "";
        return idReviewResponse;
    }

}
