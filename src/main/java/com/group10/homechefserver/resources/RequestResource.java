package com.group10.homechefserver.resources;

import com.group10.homechefserver.dao.RequestRepository;
import com.group10.homechefserver.model.Request;
import com.group10.homechefserver.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@RestController
@CrossOrigin(origins = "https://localhost:4200")
@RequestMapping("/requests")
public class RequestResource {

    @Autowired
    RequestRepository requestRepository;

    @PostMapping(path = "/addRequest")
    public String addRequest(@RequestBody Request request){
        requestRepository.save(request);
        String idRequest = request.getIdRequest() + "";
        return idRequest;
    }

    @GetMapping(path="/findById/{id}")
    public Iterable<Request> getRequestById(@PathVariable("id") int id) {
        // This returns a JSON or XML with the users
        return requestRepository.findByIdRequest(id);
    }


    @GetMapping(path="/findBySeller/{id}")
    public Iterable<Request> getRequestBySeller(@PathVariable("id") int id) {
        User u = new User();
        u.setIdUser(id);
        // This returns a JSON or XML with the users
        return requestRepository.findAllByUser(u);
    }

    @GetMapping(path="/findActiveRequests")
    public Iterable<Request> getActiveRequests() {
        // This returns a JSON or XML with the users
        return requestRepository.findByActive(true);
    }

    @GetMapping(path="/findInactiveRequests")
    public Iterable<Request> getInactiveRequests() {
        // This returns a JSON or XML with the users
        return requestRepository.findByActive(false);
    }

    //recupera Request con data più recente o uguale a quella indicata nel parametro [in questo modo: 25-05-2015]
    @GetMapping(path="/findByDateAfter/{date}")
    public Iterable<Request> findAllByDateAfter(@PathVariable String date) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Date d = sdf.parse(date);
        return requestRepository.findAllByDateAfter(d);
    }

    //recupera Request con data precedente a quella indicata nel parametro [in questo modo: 25-05-2015]
    @GetMapping(path="/findByDateBefore/{date}")
    public Iterable<Request> findAllByDateBefore(@PathVariable String date) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Date d = sdf.parse(date);
        return requestRepository.findAllByDateBefore(d);
    }

    //esempio: http://localhost:8080/requests/findByDateBeforeAndAfter/?before=17-11-2019&after=13-11-2019
    @GetMapping(path="/findByDateBeforeAndAfter/")
    public Iterable<Request> findAllByDateBefore(@RequestParam("after") String after, @RequestParam("before") String before) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Date beforeDate = sdf.parse(before);
        Date afterDate = sdf.parse(after);
        return requestRepository.findAllByDateBeforeAndDateAfter(beforeDate, afterDate);
    }

    // Ordina dalla più vecchia alla più recente
    @GetMapping(path="/getByDateOrderA")
    public Iterable<Request> findAllByOrderByDateAsc() {
        return requestRepository.findAllByOrderByDateAsc();
    }

    // Ordina dalla più recente alla più vecchia
    @GetMapping(path="/getByDateOrderD")
    public Iterable<Request> findAllByOrderByDateDesc() {
        return requestRepository.findAllByOrderByDateDesc();
    }


    @PutMapping("/edit/{id}")
    Request editRequest(@RequestBody Request newRequest, @PathVariable int id) {

        return requestRepository.findById(id)
                .map(request -> {

                    if (newRequest.getTitle() != null)
                        request.setTitle(newRequest.getTitle());

                    if (newRequest.getUser() != null)
                        request.setUser(newRequest.getUser());

                    if (newRequest.getDate() != null)
                        request.setDate(newRequest.getDate());

                    /*
                    if (newRequest.getCity() != null)
                        request.setCity(newRequest.getCity());*/

                    if (newRequest.getServings() != 0)
                        request.setServings(newRequest.getServings());

                    /*
                    if (newRequest.getExpiration() != null)
                        request.setExpiration(newRequest.getExpiration());*/

                    if (newRequest.getText() != null)
                        request.setText(newRequest.getText());

                    if (newRequest.isActive() != request.isActive())
                        request.setActive(newRequest.isActive());
                    return requestRepository.save(request);
                })
                .orElseGet(() -> {
                    newRequest.setIdRequest(id);
                    return requestRepository.save(newRequest);
                });
    }


    @DeleteMapping("/deleteRequest/{id}")
    void deleteRequest(@PathVariable int id) {
        requestRepository.deleteById(id);
    }

    @GetMapping(path="/getActiveRequestsByCity/{city}")
    public Iterable<Request> getActiveRequestsByCity(@PathVariable("city") String city) {
        // This returns a JSON or XML with the users
        return requestRepository.findAllByCityAndActive(city, true);
    }

    //http://localhost:8080/requests/getActiveRequestsByNameCourseCity/?name=carbonara&course=first&city=Torino
    @GetMapping(path="/getActiveRequestsByNameCourseCity")
    public Iterable<Request> getRequestsByNameCourseCity(@RequestParam("name") String name, @RequestParam("course") String course, @RequestParam("city") String city){
        return requestRepository.findAllByTitleContainingAndCourseAndCityAndActive(name, course, city, true);
    }

    //esempio: http://localhost:8080/requests/getActiveRequestsByCourseAndCity/?course=first&city=Torino
    @GetMapping(path="/getActiveRequestsByCourseAndCity/")
    public Iterable<Request> getActiveRequestsByCourseAndCity(@RequestParam("course") String course, @RequestParam("city") String city) {
        return requestRepository.findAllByCourseAndActiveAndCity(course, true, city);
    }



}
