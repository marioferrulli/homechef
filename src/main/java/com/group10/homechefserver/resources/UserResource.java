package com.group10.homechefserver.resources;

import com.group10.homechefserver.dao.UserRepository;
import com.group10.homechefserver.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "https://localhost:4200")
@RequestMapping("/users")
public class UserResource {

    /*
    POST:   /addUser
    GET:    /allUsers
    GET:    /findByLastName/{lastName}
    GET:    /findByName/{name}
    GET:    /findByEmail/{email}
    GET:    /findById/{id}
    PUT:    /replaceUser/{id}
    DELETE: /deleteUser/{id}
    DELETE: /deleteAllUsers

     */

    @Autowired
    UserRepository userRepository;

    @PostMapping(path="/addUser") // Map ONLY POST Requests
    public String addNewUser (@RequestBody User user) {

        User u = new User();
        u.setName(user.getName());
        u.setSurname(user.getSurname());
        u.setEmail(user.getEmail());
        userRepository.save(u);
        return "Saved";
    }

    @GetMapping(path="/allUsers")
    public Iterable<User> getAllUsers() {
        // This returns a JSON or XML with the users
        return userRepository.findAll();
    }

    @GetMapping(path="/findByLastName/{lastName}")
    public Iterable<User> getUserByLastName(@PathVariable("lastName") String lastName) {
        // This returns a JSON or XML with the users
        return userRepository.findBySurname(lastName);
    }

    @GetMapping(path="/findByName/{name}")
    public Iterable<User> getUserByName(@PathVariable("name") String name) {
        // This returns a JSON or XML with the users
        return userRepository.findByName(name);
    }

    @GetMapping(path="/findByEmail/{email}")
    public Iterable<User> getUserByEmail(@PathVariable("email") String email) {
        // This returns a JSON or XML with the users
        return userRepository.findByEmail(email);
    }

    @GetMapping(path="/findById/{id}")
    public Iterable<User> getUserById(@PathVariable("id") int id) {
        System.out.println(id);
        // This returns a JSON or XML with the users
        return userRepository.findByIdUser(id);
    }

    @PutMapping("/replaceUser/{id}")
    User replaceUser(@RequestBody User newUser, @PathVariable int id) {

        return userRepository.findById(id)
                .map(user -> {
                    user.setName(newUser.getName());
                    user.setSurname(newUser.getSurname());
                    user.setEmail(newUser.getEmail());
                    return userRepository.save(user);
                })
                .orElseGet(() -> {
                    newUser.setIdUser(id);
                    return userRepository.save(newUser);
                });
    }

    @DeleteMapping("/deleteUser/{id}")
    void deleteUser(@PathVariable int id) {
        userRepository.deleteById(id);
    }

    @DeleteMapping("/deleteAllUsers")
    void deleteAllUsers() {
        userRepository.deleteAll();
    }

}
