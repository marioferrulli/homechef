package com.group10.homechefserver.resources;

import com.group10.homechefserver.dao.PurchaseRepository;
import com.group10.homechefserver.model.Purchase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@CrossOrigin(origins = "https://localhost:4200")
@RequestMapping("/purchases")
public class PurchaseController {

    @Autowired
    PurchaseRepository purchaseRepository;

    @PostMapping(path = "/addPurchase")
    public String addPurchase(@RequestBody Purchase purchase){
        purchaseRepository.save(purchase);
        String idPurchase = purchase.getIdPurchase() + "";
        return idPurchase;
    }

    @GetMapping(path="/findBuyerByIdInsertion/{id}")
    public int getPurchaseByIdInsertion(@PathVariable("id") int idInsertion) {
        // This returns a JSON or XML with the users
        return purchaseRepository.findBuyerByInsertion(idInsertion);
    }

    @GetMapping(path="/getPurchaseDate/")
    public Date getPurchaseDate(@RequestParam("insertion") int insertion, @RequestParam("buyer") int buyer) {
        // This returns a JSON or XML with the users
        return purchaseRepository.getPurchaseDateByBuyerAndInsertion(buyer, insertion);
    }




}
