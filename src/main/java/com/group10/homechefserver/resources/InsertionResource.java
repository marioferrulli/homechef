package com.group10.homechefserver.resources;

import com.group10.homechefserver.dao.InsertionRepository;
import com.group10.homechefserver.model.Insertion;
import com.group10.homechefserver.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@RestController
@CrossOrigin(origins = "https://localhost:4200")
@RequestMapping("/insertions")
public class InsertionResource {

    /*
    POST:   /addInsertion
    GET:    /findActiveInsertions
    GET:    /findInactiveInsertions
    DELETE: /deleteInsertion/{id}
    GET:    /findById/{id}
    GET:    /findBySeller/{id}
    PUT:    /edit/{id}
    GET:    /findByDateBefore/{date}
    GET:    /findByDateAfter/{date}
    GET:    /findByDateBeforeAndAfter/?before & ?after
    GET:    /findByPriceLessThanOrEqualTo/{price}
    GET:    /getByPriceOrderD
    GET:    /getByPriceOrderA
    GET:    /getActiveInsertionsByCity/{city}
    GET:    /getActiveInsertionsByCourse/{course}
    GET:    /getActiveInsertionsByCourseAndCity/?course=first&city=Torino

    */

    @Autowired
    InsertionRepository insertionRepository;

    @PostMapping(path = "/addInsertion")
    public String addInsertion(@RequestBody Insertion insertion){
        insertionRepository.save(insertion);
        String idInsertion = insertion.getIdInsertion() + "";
        return idInsertion;
    }

    @GetMapping(path="/findById/{id}")
    public Iterable<Insertion> getInsertionById(@PathVariable("id") int id) {
        // This returns a JSON or XML with the users
        return insertionRepository.findByIdInsertion(id);
    }


    @GetMapping(path="/findBySeller/{id}")
    public Iterable<Insertion> getInsertionBySeller(@PathVariable("id") int id) {
        User u = new User();
        u.setIdUser(id);
        // This returns a JSON or XML with the users
        return insertionRepository.findAllByUser(u);
    }

    @GetMapping(path="/findActiveInsertions")
    public Iterable<Insertion> getActiveInsertions() {
        // This returns a JSON or XML with the users
        return insertionRepository.findByPublished(true);
    }

    @GetMapping(path="/findInactiveInsertions")
    public Iterable<Insertion> getInctiveInsertions() {
        // This returns a JSON or XML with the users
        return insertionRepository.findByPublished(false);
    }

    //recupera inserzioni con data più recente o uguale a quella indicata nel parametro [in questo modo: 25-05-2015]
    @GetMapping(path="/findByDateAfter/{date}")
    public Iterable<Insertion> findAllByDateAfter(@PathVariable String date) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Date d = sdf.parse(date);
        return insertionRepository.findAllByDateAfter(d);
    }

    //recupera inserzioni con data precedente a quella indicata nel parametro [in questo modo: 25-05-2015]
    @GetMapping(path="/findByDateBefore/{date}")
    public Iterable<Insertion> findAllByDateBefore(@PathVariable String date) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Date d = sdf.parse(date);
        return insertionRepository.findAllByDateBefore(d);
    }

    //esempio: http://localhost:8080/insertions/findByDateBeforeAndAfter/?before=17-11-2019&after=13-11-2019
    @GetMapping(path="/findByDateBeforeAndAfter/")
    public Iterable<Insertion> findAllByDateBefore(@RequestParam("after") String after, @RequestParam("before") String before) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Date beforeDate = sdf.parse(before);
        Date afterDate = sdf.parse(after);
        return insertionRepository.findAllByDateBeforeAndDateAfter(beforeDate, afterDate);
    }


    // Ordina dalla più vecchia alla più recente
    @GetMapping(path="/getByDateOrderA")
    public Iterable<Insertion> findAllByOrderByDateAsc() {
        return insertionRepository.findAllByOrderByDateAsc();
    }

    // Ordina dalla più recente alla più vecchia
    @GetMapping(path="/getByDateOrderD")
    public Iterable<Insertion> findAllByOrderByDateDesc() {
        return insertionRepository.findAllByOrderByDateDesc();
    }

    //
    @GetMapping(path="/findByPriceLessThanOrEqualTo/{price}")
    public Iterable<Insertion> findAllByPriceLessThanEqual(@PathVariable double price) {
        return insertionRepository.findAllByPriceLessThanEqual(price);
    }

    // Ordina dalla più economica alla più costosa
    @GetMapping(path="/getByPriceOrderA")
    public Iterable<Insertion> findAllByOrderByPriceAsc() {
        return insertionRepository.findAllByOrderByPriceAsc();
    }

    // Ordina dalla più costosa alla più economica
    @GetMapping(path="/getByPriceOrderD")
    public Iterable<Insertion> findAllByOrderByPriceDesc() {
        return insertionRepository.findAllByOrderByPriceDesc();
    }

    @Transactional
    @PutMapping("/editStatus/")
    public String editStatusInsertion(@RequestParam("published") boolean published, @RequestParam("idInsertion") int idInsertion){
        insertionRepository.updateInsertionStatus(published, idInsertion);
        return "Updated";
    }

    @PutMapping("/edit/{id}")
    Insertion editInsertion(@RequestBody Insertion newInsertion, @PathVariable int id) {

        return insertionRepository.findById(id)
                .map(insertion -> {

                    if (newInsertion.getTitle() != null)
                        insertion.setTitle(newInsertion.getTitle());

                    if (newInsertion.getUser() != null)
                        insertion.setUser(newInsertion.getUser());

                    if (newInsertion.getDate() != null)
                        insertion.setDate(newInsertion.getDate());

                    if (newInsertion.getCity() != null)
                        insertion.setCity(newInsertion.getCity());

                    if (newInsertion.getServings() != 0)
                        insertion.setServings(newInsertion.getServings());

                    if (newInsertion.getExpiration() != null)
                        insertion.setExpiration(newInsertion.getExpiration());

                    if (newInsertion.getIngredients() != null)
                        insertion.setIngredients(newInsertion.getIngredients());

                    if (newInsertion.getPrice() != 0)
                        insertion.setPrice(newInsertion.getPrice());

                    if (newInsertion.getDescription() != null)
                        insertion.setDescription(newInsertion.getDescription());

                    if (newInsertion.isPublished() != insertion.isPublished())
                        insertion.setPublished(newInsertion.isPublished());
                    return insertionRepository.save(insertion);
                })
                .orElseGet(() -> {
                    newInsertion.setIdInsertion(id);
                    return insertionRepository.save(newInsertion);
                });
    }


    @DeleteMapping("/deleteInsertion/{id}")
    void deleteInsertion(@PathVariable int id) {
        insertionRepository.deleteById(id);
        final File uploadDirRoot = new File("src/main/resources/img/");
        File toDelete = new File(uploadDirRoot, "insertion"+Long.toString(id));
        toDelete.delete();
    }

    @GetMapping(path="/getActiveInsertionsByCity/{city}")
    public Iterable<Insertion> getActiveInsertionsByCity(@PathVariable("city") String city) {
        // This returns a JSON or XML with the users
        return insertionRepository.findAllByCityAndPublished(city, true);
    }

    //http://localhost:8080/insertions/getActiveInsertionsByNameCourseCity/?name=carbonara&course=first&city=Torino
    @GetMapping(path="/getActiveInsertionsByNameCourseCity")
    public Iterable<Insertion> getInsertionsByNameCourseCity(@RequestParam("name") String name, @RequestParam("course") String course, @RequestParam("city") String city){
        return insertionRepository.findAllByTitleContainingAndCourseAndCityAndPublished(name, course, city, true);
    }

    @GetMapping(path="/getActiveInsertionsByNameAndCity")
    public Iterable<Insertion> getInsertionsByNameAndCity(@RequestParam("name") String name, @RequestParam("city") String city){
        return insertionRepository.findAllByTitleContainingAndCityAndPublished(name, city, true);
    }

    @GetMapping(path="/getActiveInsertionsByNameAndCourse")
    public Iterable<Insertion> getInsertionsByNameAndCourse(@RequestParam("name") String name, @RequestParam("course") String course){
        return insertionRepository.findAllByTitleContainingAndCourseAndPublished(name, course, true);
    }

    @GetMapping(path="/getActiveInsertionsByName")
    public Iterable<Insertion> getInsertionsByName(@RequestParam("name") String name){
        return insertionRepository.findAllByTitleContainingAndPublished(name, true);
    }


    @GetMapping(path="/getActiveInsertionsByCourse/{course}")
    public Iterable<Insertion> getActiveInsertionsByCourse(@PathVariable("course") String course) {
        String c = "";
        if (course.compareToIgnoreCase("first") == 0)
            c = "first";
        else if (course.compareToIgnoreCase("second") == 0)
            c = "second";
        else if (course.compareToIgnoreCase("fruit") == 0)
            c = "fruit";
        else if (course.compareToIgnoreCase("dessert") == 0)
            c = "dessert";
        else if (course.compareToIgnoreCase("appetizer") == 0)
            c = "appetizer";
        else
            c = "unknown";
        return insertionRepository.findAllByCourseAndPublished(c, true);
    }

    //esempio: http://localhost:8080/insertions/getActiveInsertionsByCourseAndCity/?course=first&city=Torino
    @GetMapping(path="/getActiveInsertionsByCourseAndCity/")
    public Iterable<Insertion> getActiveInsertionByCourseAndCity(@RequestParam("course") String course, @RequestParam("city") String city) {
        return insertionRepository.findAllByCourseAndPublishedAndCity(course, true, city);
    }


    @GetMapping(path="/getInsertionStatus/{id}")
    public boolean getInsertionStatusByIdInsertion(@PathVariable("id") int idInsertion) {
        // This returns a JSON or XML with the users
        return insertionRepository.getStatusByInsertion(idInsertion);
    }


    @GetMapping(path="/findInsertionsByBuyer/{id}")
    public Iterable<Insertion> getInsertionsByBuyer(@PathVariable("id") int idBuyer) {
        // This returns a JSON or XML with the users
        return insertionRepository.findAllByBuyer(idBuyer);
    }

}
