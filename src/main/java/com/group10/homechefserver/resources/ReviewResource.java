package com.group10.homechefserver.resources;

import com.group10.homechefserver.dao.ReviewRepository;
import com.group10.homechefserver.model.Review;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@RestController
@CrossOrigin(origins = "https://localhost:4200")
@RequestMapping("/reviews")
public class ReviewResource {

    /*
    POST:   /addReview
    DELETE: /deleteReview/{id}
    GET:    /findById/{id}
    GET:    /findReceivedReviews/{id}
    GET:    /findByDateBefore/{date}
    GET:    /findByDateAfter/{date}
    GET:    /findByDateBeforeAndAfter/?before & ?after
    PUT:    /edit/{id}
     */

    @Autowired
    ReviewRepository reviewRepository;

    @PostMapping(path="/addReview") // Map ONLY POST Requests
    public String addNewReview (@RequestBody Review review) {
        // @ResponseBody non necessario
        // @RequestParam sono parametri del path del tipo ?name=mario&surname=ferrulli
        // @PathVariable indica la variabile tra parentesi graffe da sostituire

        reviewRepository.save(review);
        String idReview = review.getIdReview() + "";
        return idReview;
    }

    @DeleteMapping("/deleteReview/{id}")
    void deleteReview(@PathVariable int id) {
        reviewRepository.deleteById(id);
    }

    @GetMapping(path="/findById/{id}")
    public Iterable<Review> getReviewById(@PathVariable("id") int id) {
        // This returns a JSON or XML with the users
        return reviewRepository.findByIdReview(id);
    }

    @GetMapping(path="/findReviewByUserAndInsertion/")
    public Integer findReviewByUserAndInsertion(@RequestParam("user") int user, @RequestParam("insertion") int insertion) throws ParseException {
        return reviewRepository.findReviewByUserAndInsertion(user, insertion);
    }

    @GetMapping(path="/getAverageRatingForUser/{idUser}")
    public double findAverage(@PathVariable("idUser") int idUser){
        return reviewRepository.getAverageRatingForUser(idUser);
    }


    @GetMapping(path="/findReceivedReviews/{id}")
    public Iterable<Review> getReceivedReviews(@PathVariable("id") int id) {
        // This returns a JSON or XML with the users
        return reviewRepository.findAllByReceiverUser(id);
    }

    //recupera inserzioni con data più recente o uguale a quella indicata nel parametro [in questo modo: 25-05-2015]
    @GetMapping(path="/findByDateAfter/{date}")
    public Iterable<Review> findAllByDateAfter(@PathVariable String date) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Date d = sdf.parse(date);
        return reviewRepository.findAllByDateAfter(d);
    }

    //recupera inserzioni con data precedente a quella indicata nel parametro [in questo modo: 25-05-2015]
    @GetMapping(path="/findByDateBefore/{date}")
    public Iterable<Review> findAllByDateBefore(@PathVariable String date) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Date d = sdf.parse(date);
        return reviewRepository.findAllByDateBefore(d);
    }

    //esempio: http://localhost:8080/insertions/findByDateBeforeAndAfter/?before=17-11-2019&after=13-11-2019
    @GetMapping(path="/findByDateBeforeAndAfter/")
    public Iterable<Review> findAllByDateBefore(@RequestParam("after") String after, @RequestParam("before") String before) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Date beforeDate = sdf.parse(before);
        Date afterDate = sdf.parse(after);
        return reviewRepository.findAllByDateBeforeAndDateAfter(beforeDate, afterDate);
    }

    @PutMapping("/edit/{id}")
    public Review editReview(@RequestBody Review newReview, @PathVariable int id) {

        return reviewRepository.findById(id)
                .map(review -> {

                    if (newReview.getInsertion() != null)
                        review.setInsertion(newReview.getInsertion());

                    if (newReview.getUser() != null)
                        review.setUser(newReview.getUser());

                    if (newReview.getRating() != 0)
                        review.setDate(newReview.getDate());

                    if (newReview.getText() == null)
                        review.setText(newReview.getText());

                    if (newReview.getDate() != null)
                        review.setDate(newReview.getDate());
                    return reviewRepository.save(review);
                })
                .orElseGet(() -> {
                    newReview.setIdReview(id);
                    return reviewRepository.save(newReview);
                });
    }




}
