package com.group10.homechefserver.resources;

import java.io.*;
import java.net.URI;
import java.util.concurrent.Callable;

import com.group10.homechefserver.dao.InsertionRepository;
import com.group10.homechefserver.model.Insertion;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;


@RestController
@RequestMapping(value = "/insertions-images")
@PropertySource("classpath:application.properties")
@CrossOrigin(origins = "https://localhost:4200")

public class InsertionImageController {

    @Autowired
    private InsertionRepository repository;

    private static final File uploadDirRoot = new File("img/");

    @Autowired
    InsertionImageController(InsertionRepository repository) {
        this.repository = repository;
    }

    @GetMapping(path = "/get/{id}")
    ResponseEntity<Resource> read(@PathVariable int id) throws Exception {


        return this.repository.findById(id)
                .map(insertion -> {
                    String path = null;
                    path = pathForInsertion(insertion);

                    System.out.println("PATH: " + path);


                    //InputStream is = this.getClass().getClassLoader().getResourceAsStream(path);
                    //Resource fileSystemResource = new InputStreamResource(is);

                    File file = new File(path);
                    Resource fileSystemResource = new FileSystemResource(file);

                    return ResponseEntity.ok()
                            .contentType(MediaType.IMAGE_JPEG)
                            .body(fileSystemResource);
                })
                .orElseThrow(() -> new Exception("Image for available"));
    }


    @RequestMapping(value = "/post/{id}", method = { RequestMethod.POST, RequestMethod.PUT },
            consumes = { "multipart/form-data" })
    Callable<ResponseEntity<?>> write(@PathVariable int id,
                                      @RequestParam("file") MultipartFile file) throws Exception {

        return new Callable<ResponseEntity<?>>() {
            @Override
            public ResponseEntity<?> call() throws Exception {
                return InsertionImageController.this.repository.findById(id)
                        .map(insertion ->
                        {
                            String path = null;
                            path = InsertionImageController.this.pathForInsertion2(insertion);



                            System.out.println("Sto scrivendo il file in: " + path);
                            File convFile = null; // choose your own extension I guess? Filename accessible with convFile.getAbsolutePath()
                            FileOutputStream fos = null;

                            //InputStream is = this.getClass().getClassLoader().getResourceAsStream(path);

                            try {
                                convFile = new File(path);
                                System.out.println("Creato file");
                                if (!convFile.exists()) {
                                    System.out.println("Non esiste, quindi lo creo.");
                                    convFile.createNewFile();
                                    System.out.println("Creato");
                                }
                                fos = new FileOutputStream(convFile);
                                System.out.println("Aperto fileoutpustream");
                                fos.write(file.getBytes());
                                System.out.println("fatto getBytes");
                                fos.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            URI location = fromCurrentRequest().buildAndExpand(id).toUri();
                            return ResponseEntity.created(location).build();
                        })
                        .orElseThrow(() -> new Exception("Insertion id is not present in database"));
            }
        };
    }



    private String pathForInsertion(Insertion e) {
        return "../myStorage/insertion" + Long.toString(e.getIdInsertion());
    }

    private String pathForInsertion2(Insertion e) {
        return "../myStorage/insertion" + Long.toString(e.getIdInsertion());
    }
}