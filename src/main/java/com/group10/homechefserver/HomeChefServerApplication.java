package com.group10.homechefserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;


import org.springframework.web.client.RestTemplate;



@SpringBootApplication
@EnableEurekaClient
public class HomeChefServerApplication {

    @Bean
    @LoadBalanced
    //in questo modo diciamo a RestTemplate di non andare direttamente ad un service qualunque sia l'URL che sto dando
    //perchè non è un reale URL ma è solamente un suggerimento riguardo il service che deve scoprire
    // + Load Balancing
    public RestTemplate getRestTemplate(){
        return new RestTemplate();
    }

    public static void main(String[] args) {
        SpringApplication.run(HomeChefServerApplication.class, args);
    }



}
