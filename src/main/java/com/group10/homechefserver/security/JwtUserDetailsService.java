package com.group10.homechefserver.security;

import com.group10.homechefserver.dao.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;


    //forse inutile perchè non abbiamo bisogno della password
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        List<com.group10.homechefserver.model.User> userFound = userRepository.findByEmail(username);
        String email = "";
        if(!userFound.isEmpty()) {
            email = userFound.get(0).getEmail();
            return new User(email, "", new ArrayList<>());
        }

        else
            throw new UsernameNotFoundException("User not found with username: " + username);

    }
}
