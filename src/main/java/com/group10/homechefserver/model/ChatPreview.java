package com.group10.homechefserver.model;

public class ChatPreview {

    private String nameContact;
    private int idContact;
    private String lastMessage;

    public ChatPreview(String nameContact, int idContact, String lastMessage) {
        this.nameContact = nameContact;
        this.idContact = idContact;
        this.lastMessage = lastMessage;
    }

    public ChatPreview() {
    }

    public String getNameContact() {
        return nameContact;
    }

    public void setNameContact(String nameContact) {
        this.nameContact = nameContact;
    }

    public int getIdContact() {
        return idContact;
    }

    public void setIdContact(int idContact) {
        this.idContact = idContact;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }
}
