package com.group10.homechefserver.model;

import java.util.List;

public class Chat {

    private List<Message> messageList;

    public Chat(List<Message> messageList) {
        this.messageList = messageList;
    }

    public Chat() {
    }

    public List<Message> getMessageList() {
        return messageList;
    }

    public void setMessageList(List<Message> messageList) {
        this.messageList = messageList;
    }
}
