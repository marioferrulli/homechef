package com.group10.homechefserver.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "REQUESTS")
public class Request {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idRequest;

    @ManyToOne(optional = false, cascade = {CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "ID_USER")
    private User user;

    private String title;

    private String city;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date date;

    @PrePersist
    public void addTimestamp() {
        date = new Date();
    }

    private int servings;

    private String expiration;

    private String text;
    private boolean active;

    private String course;

    public Request(User user, String title, String city, Date date, int servings, String expiration, String text, boolean active, String course) {
        this.user = user;
        this.title = title;
        this.city = city;
        this.date = date;
        this.servings = servings;
        this.expiration = expiration;
        this.text = text;
        this.active = active;
        this.course = course;
    }

    public Request() {
    }

    public String getExpiration() {
        return expiration;
    }

    public void setExpiration(String expiration) {
        this.expiration = expiration;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getIdRequest() {
        return idRequest;
    }

    public void setIdRequest(int idRequest) {
        this.idRequest = idRequest;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getServings() {
        return servings;
    }

    public void setServings(int servings) {
        this.servings = servings;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
