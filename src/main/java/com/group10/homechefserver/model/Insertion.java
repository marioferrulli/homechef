package com.group10.homechefserver.model;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "INSERTIONS")
public class Insertion {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idInsertion;
    private String title;

    @ManyToOne(optional = false, cascade = {CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "ID_USER")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private User user;

    /*
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date date;

    */

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date date;

    @PrePersist
    public void addTimestamp() {
        date = new Date();
    }

    private String city;
    private int servings;
    private String expiration;

    @ElementCollection
    private List<String> ingredients;

    private double price;
    private String description;
    private boolean published;

    private String course;




    /*
    public Insertion(String title, User user, Date date, String city, int servings, String expiration, List<String> ingredients, double price, String description, boolean published) {
        this.title = title;
        this.user = user;
        this.date = date;
        this.city = city;
        this.servings = servings;
        this.expiration = expiration;
        this.ingredients = ingredients;
        this.price = price;
        this.description = description;
        this.published = published;
    }

     */

    public Insertion(String title, User user, Date date, String city, int servings, String expiration, List<String> ingredients, double price, String description, boolean published, String course) {
        this.title = title;
        this.user = user;
        this.date = date;
        this.city = city;
        this.servings = servings;
        this.expiration = expiration;
        this.ingredients = ingredients;
        this.price = price;
        this.description = description;
        this.published = published;
        this.course = course;
    }

    public Insertion() {
    }

    public int getIdInsertion() {
        return idInsertion;
    }

    public void setIdInsertion(int idInsertion) {
        this.idInsertion = idInsertion;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getServings() {
        return servings;
    }

    public void setServings(int servings) {
        this.servings = servings;
    }

    public String getExpiration() {
        return expiration;
    }

    public void setExpiration(String expiration) {
        this.expiration = expiration;
    }

    public List<String> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<String> ingredients) {
        this.ingredients = ingredients;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }
}



