package com.group10.homechefserver.model;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table(name = "RESPONSES")
public class ReviewResponse {

    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    private int idResponse;

    @ManyToOne(optional = false, cascade = {CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "ID_USER")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private User user;

    @OneToOne
    @JoinColumn(name = "ID_REVIEW")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Review review;

    private String text;

    public ReviewResponse(User user, Review review, String text) {
        this.user = user;
        this.review = review;
        this.text = text;
    }

    public ReviewResponse() {
    }

    public int getIdResponse() {
        return idResponse;
    }

    public void setIdResponse(int idResponse) {
        this.idResponse = idResponse;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Review getReview() {
        return review;
    }

    public void setReview(Review review) {
        this.review = review;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
