package com.group10.homechefserver.model;

import java.util.List;

public class Conversations {

    private List<Message> conversations;

    public Conversations(List<Message> conversations) {
        this.conversations = conversations;
    }

    public Conversations() {
    }

    public List<Message> getConversations() {
        return conversations;
    }

    public void setConversations(List<Message> conversations) {
        this.conversations = conversations;
    }
}
