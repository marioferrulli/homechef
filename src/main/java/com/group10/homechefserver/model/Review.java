package com.group10.homechefserver.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "REVIEWS")
public class Review {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "ID_REVIEW")
    private int idReview;

    @OneToOne(optional=false)
    @JoinColumn(name = "ID_INSERTION")
    @OnDelete(action = OnDeleteAction.CASCADE)
    //questo cancella la review nel caso dovesse essere cancellata una insertion
    private Insertion insertion;

    @ManyToOne(optional=false, cascade = {CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "ID_USER")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private User user;

    private double rating;

    private String text;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date date;

    @PrePersist
    public void addTimestamp() {
        date = new Date();
    }

    public Review(Insertion insertion, User user, double rating, String text, Date date) {
        this.insertion = insertion;
        this.user = user;
        this.rating = rating;
        this.text = text;
        this.date = date;
    }

    public Review() {
    }

    public int getIdReview() {
        return idReview;
    }

    public void setIdReview(int idReview) {
        this.idReview = idReview;
    }

    public Insertion getInsertion() {
        return insertion;
    }

    public void setInsertion(Insertion insertion) {
        this.insertion = insertion;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}