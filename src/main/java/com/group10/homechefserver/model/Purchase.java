package com.group10.homechefserver.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "PURCHASES")
public class Purchase {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idPurchase;

    @ManyToOne(optional = false, cascade = {CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "ID_USER")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private User user;

    @OneToOne
    @JoinColumn(name = "ID_INSERTION")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Insertion insertion;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date date;

    @PrePersist
    public void addTimestamp() {
        date = new Date();
    }

    public Purchase(User user, Insertion insertion, Date date) {
        this.user = user;
        this.insertion = insertion;
        this.date = date;
    }

    public Purchase() {
    }

    public int getIdPurchase() {
        return idPurchase;
    }

    public void setIdPurchase(int idPurchase) {
        this.idPurchase = idPurchase;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Insertion getInsertion() {
        return insertion;
    }

    public void setInsertion(Insertion insertion) {
        this.insertion = insertion;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
