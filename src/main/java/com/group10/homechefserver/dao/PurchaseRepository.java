package com.group10.homechefserver.dao;

import com.group10.homechefserver.model.Purchase;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;

public interface PurchaseRepository extends CrudRepository<Purchase, Integer> {

    @Query(value = "select P.id_user from purchases P where P.id_insertion = :idInsertion", nativeQuery = true)
    int findBuyerByInsertion(int idInsertion);

    @Query(value = "select P.date from purchases P where P.id_insertion = :insertion and P.id_user = :buyer", nativeQuery = true)
    Date getPurchaseDateByBuyerAndInsertion(int buyer, int insertion);


    //@Query("update Insertion i set i.published = ?1 where i.idInsertion = ?2")

    /*
    @Query(value = "SELECT " +
            " R.id_review, " +
            "    R.date,\n" +
            "    R.rating,\n" +
            "    R.text,\n" +
            "    R.id_insertion,\n" +
            "    R.id_user\n" +
            "FROM\n" +
            "\tREVIEWS AS R \n" +
            "\tJOIN INSERTIONS AS I ON I.ID_INSERTION = R.ID_INSERTION\n" +
            "\tJOIN USERS AS U ON I.ID_USER = U.ID_USER\n" +
            "WHERE\n" +
            "\tI.ID_USER = :id\n", nativeQuery = true)    //This is using a named query method
     */

}
