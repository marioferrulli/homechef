package com.group10.homechefserver.dao;

import com.group10.homechefserver.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Integer> {


    List<User> findBySurname(String lastName);
    List<User> findByIdUser(int id);
    List<User> findByName(String name);
    List<User> findByEmail(String email);
    List<User> findByFacebookId(String id);
    List<User> findByGithubId(String id);

}

