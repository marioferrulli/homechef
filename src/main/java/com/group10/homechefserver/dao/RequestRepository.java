package com.group10.homechefserver.dao;

import com.group10.homechefserver.model.Insertion;
import com.group10.homechefserver.model.Request;
import com.group10.homechefserver.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;

public interface RequestRepository extends CrudRepository<Request, Integer> {

    Iterable<Request> findByIdRequest(int id);

    Iterable<Request> findAllByUser(User u);

    Iterable<Request> findByActive(boolean b);

    Iterable<Request> findAllByDateAfter(Date d);

    Iterable<Request> findAllByDateBefore(Date d);

    Iterable<Request> findAllByDateBeforeAndDateAfter(Date beforeDate, Date afterDate);

    Iterable<Request> findAllByOrderByDateAsc();

    Iterable<Request> findAllByOrderByDateDesc();

    Iterable<Request> findAllByCityAndActive(String city, boolean b);

    Iterable<Request> findAllByTitleContainingAndCourseAndCityAndActive(String name, String course, String city, boolean b);

    Iterable<Request> findAllByCourseAndActiveAndCity(String course, boolean b, String city);
}
