package com.group10.homechefserver.dao;

import com.group10.homechefserver.model.Insertion;
import com.group10.homechefserver.model.User;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface InsertionRepository extends CrudRepository<Insertion, Integer> {

    List<Insertion> findByPublished(boolean published);

    List<Insertion> findByIdInsertion(int id);

    List<Insertion> findAllByUser(User user);

    List<Insertion> findAllByOrderByDateAsc();

    List<Insertion> findAllByOrderByDateDesc();

    List<Insertion> findAllByDateAfter(Date d);

    Iterable<Insertion> findAllByDateBefore(Date d);

    Iterable<Insertion> findAllByDateBeforeAndDateAfter(Date beforeDate, Date afterDate);

    Iterable<Insertion> findAllByPriceLessThanEqual(double price);

    Iterable<Insertion> findAllByOrderByPriceAsc();

    Iterable<Insertion> findAllByOrderByPriceDesc();

    Iterable<Insertion> findAllByCityAndPublished(String city, boolean published);

    Iterable<Insertion> findAllByCourseAndPublished(String course, boolean b);

    Iterable<Insertion> findAllByCourseAndPublishedAndCity(String course, boolean b, String city);

    Iterable<Insertion> findAllByTitleContainingAndCourseAndCityAndPublished(String title, String course, String city, boolean published);

    Iterable<Insertion> findAllByTitleContainingAndCityAndPublished(String title, String city, boolean b);

    Iterable<Insertion> findAllByTitleContainingAndCourseAndPublished(String title, String course, boolean b);

    Iterable<Insertion> findAllByTitleContainingAndPublished(String title, boolean b);

    @Modifying
    @Query("update Insertion i set i.published = ?1 where i.idInsertion = ?2")
    void updateInsertionStatus(boolean status, int idInsertion);

    @Query(value = "select I.published from insertions I where I.id_insertion = :idInsertion", nativeQuery = true)
    boolean getStatusByInsertion(int idInsertion);

    @Query(value = "select i.id_insertion, i.city, i.course, i.date, i.description, i.expiration, i.price, i.published, i.servings, i.title, i.id_user from insertions i join purchases p on i.id_insertion = p.id_insertion where p.id_user = :idBuyer", nativeQuery = true)
    Iterable<Insertion> findAllByBuyer(int idBuyer);
}
