package com.group10.homechefserver.dao;

import com.group10.homechefserver.model.Review;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface ReviewRepository extends CrudRepository<Review, Integer> {
    Iterable<Review> findByIdReview(int id);

    @Query(value = "SELECT\n" +
            "\tR.id_review,\n" +
            "    R.date,\n" +
            "    R.rating,\n" +
            "    R.text,\n" +
            "    R.id_insertion,\n" +
            "    R.id_user\n" +
            "FROM\n" +
            "\treviews AS R \n" +
            "\tJOIN insertions AS I ON I.id_insertion = R.id_insertion\n" +
            "\tJOIN users AS U ON I.id_user = U.id_user\n" +
            "WHERE\n" +
            "\tI.id_user = :id\n", nativeQuery = true)    //This is using a named query method
    public List<Review> findAllByReceiverUser(Integer id);

    @Query(value = "select avg(r.rating) from reviews as r join insertions as i on i.id_insertion = r.id_insertion join users as u on i.id_user = u.id_user where i.id_user = :idUser", nativeQuery = true)
    public double getAverageRatingForUser(int idUser);


    Iterable<Review> findAllByDateAfter(Date d);

    Iterable<Review> findAllByDateBefore(Date d);

    Iterable<Review> findAllByDateBeforeAndDateAfter(Date beforeDate, Date afterDate);

    @Query(value = "SELECT\n" +
            "\tid_review\n" +
            "FROM\n" +
            "\treviews\n" +
            "WHERE\n" +
            "\tid_user = :userId\n" +
            "AND id_insertion = :insertionId", nativeQuery = true)    //This is using a named query method
    Integer findReviewByUserAndInsertion(int userId, int insertionId);

}

/*
"SELECT\n" +
            "\tR.ID_REVIEW AS REVIEW_ID,\n" +
            "    R.DATE AS REVIEW_DATE,\n" +
            "    R.RATING AS REVIEW_RATING,\n" +
            "    R.TEXT AS REVIEW_TEXT,\n" +
            "    I.ID_INSERTION AS INSERTION_ID,\n" +
            "    U.ID_USER AS USER_ID\n" +
            "FROM\n" +
            "\tREVIEWS AS R \n" +
            "\tJOIN INSERTIONS AS I ON I.ID_INSERTION = R.ID_INSERTION\n" +
            "\tJOIN USERS AS U ON I.ID_USER = U.ID_USER\n" +
            "WHERE\n" +
            "\tI.ID_USER = :id"
 */
