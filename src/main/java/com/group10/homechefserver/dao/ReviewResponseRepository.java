package com.group10.homechefserver.dao;

import com.group10.homechefserver.model.ReviewResponse;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface ReviewResponseRepository extends CrudRepository<ReviewResponse, Integer> {

    @Query(value = "SELECT\n" +
            "\tid_user,\n" +
            "\t id_response,\n" +
            "\t text,\n" +
            "\t id_review\n" +
            "FROM\n" +
            "\tresponses\n" +
            "WHERE\n" +
            "\tid_user = :idUser AND id_review = :idReview\n", nativeQuery = true)    //This is using a named query method
    Iterable<ReviewResponse> findResponsesByReviewAndUser(Integer idReview, Integer idUser);

}
