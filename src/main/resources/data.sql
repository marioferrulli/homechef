-- noinspection SqlNoDataSourceInspectionForFile
INSERT INTO users(id_user,email,name,surname, facebook_id) values (777,'mario.juggler@hotmail.it', 'Mario', 'Ferrulli', '10220459974638616');
INSERT INTO users(id_user,email,name,surname, facebook_id) values (888,'alinamansukhlal@gmail.com', 'Alina', 'Mansukhlal', '2518446935079832');
INSERT INTO users(id_user,email,name,surname) values (999,'matteomura@gmail.com', 'Matteo', 'Mura');

INSERT INTO insertions values (
	101,
    'Torino',
    'first',
    '2019-11-17 23.31',
    'Ricetta originale romana',
    '3 giorni',
    20.00,
    true,
    4,
    'Carbonara',
    777
);


INSERT INTO insertions values (
	102,
    'Milano',
    'first',
    '2019-11-17 22.01',
    'Ricetta originale napoletana',
    '4 giorni',
    10.00,
    true,
    6,
    'Pizza Margherita',
    888
);

INSERT INTO insertions values (
	103,
    'Torino',
    'second',
    '2019-11-18 12.00',
    'Ricetta originale piemontese',
    '4 giorni',
    15.00,
    true,
    2,
    'Bagna Cauda',
    888
);


/*INSERT INTO reviews values (
	1,
    '2019-11-19 6.34',
    3.5,
    'Buona.',
    3,
    777
);

INSERT INTO reviews values (
	2,
    '2019-11-10 16.54',
    4.5,
    'Ottima.',
    1,
    999
);

INSERT INTO reviews values (
	3,
    '2019-11-14 18.44',
    5,
    'Spettacolare.',
    2,
    888
);

 */

INSERT INTO insertion_ingredients values (101,'Pecorino');
INSERT INTO insertion_ingredients values (101,'Guanciale');
INSERT INTO insertion_ingredients values (101,'Uova');
INSERT INTO insertion_ingredients values (102,'Mozzarella');
INSERT INTO insertion_ingredients values (102,'Pomodoro');
INSERT INTO insertion_ingredients values (102,'Basilico');
INSERT INTO insertion_ingredients values (103,'Aglio');
INSERT INTO insertion_ingredients values (103,'Interiora');
INSERT INTO insertion_ingredients values (103,'Olio');




