import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient, HttpEvent, HttpHeaders, HttpParams, HttpRequest} from "@angular/common/http";
import {Insertion} from "./insertion";
import {FormGroup} from "@angular/forms";
import {map, retry} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class InsertionServiceService {

  public insertionUrl1: string;
  public insertionUrl2: string;
  public insertionUrlbyId: string;
  public insertionUrlbycity: string;
  public insertionUrlbycourseandcity: string;
  public insertionUrlbynamecourseandcity: string;
  public insertionUrlbycourse: string;
  public insertionUrlbynameandcity: string;
  public insertionUrlbycourseandname: string;
  public jwt;
  public insertionUrlbyname: string;
  public addInsertion: string;
  public idInsertion: number;
  public addImageUrl: string;
  public getImageUrl: string;
  public insertionUrlBySeller: string;
  public deleteInsertion: string;
  public urlEditStatusInsertion: string;
  public urlToOrderByDateDecrescente: string;
  public urlToOrderByDateCrescente: string;
  public urlToOrderbyPriceD:string;
  public urlToOrderbyProceA: string;
  public utlToEditInsertion: string;

  constructor(private  http: HttpClient) {
    this.insertionUrl1 = "http://localhost:8080/insertions/findActiveInsertions";
    this.insertionUrl2 = "http://localhost:8080/insertions/findInactiveInsertions";
    this.insertionUrlbyId = "http://localhost:8080/insertions/findById";
    this.insertionUrlbycity = "http://localhost:8080/insertions/getActiveInsertionsByCity";
    this.insertionUrlbycourseandcity = "http://localhost:8080/insertions/getActiveInsertionsByCourseAndCity";
    this.insertionUrlbynameandcity = "http://localhost:8080/insertions/getActiveInsertionsByNameAndCity"
    this.insertionUrlbynamecourseandcity = "http://localhost:8080/insertions/getActiveInsertionsByNameCourseCity";
    this.insertionUrlbycourse = "http://localhost:8080/insertions/getActiveInsertionsByCourse";
    this.insertionUrlbycourseandname = "http://localhost:8080/insertions/getActiveInsertionsByNameAndCourse"
    this.insertionUrlbyname = "http://localhost:8080/insertions/getActiveInsertionsByName";
    this.addInsertion = "http://localhost:8080/insertions/addInsertion";
    this.addImageUrl = "http://localhost:8080/insertions-images/post/";
    this.getImageUrl= "http://localhost:8080/insertions-images/get/";
    this.insertionUrlBySeller= "http://localhost:8080/insertions/findBySeller/";
    this.deleteInsertion="http://localhost:8080/insertions/deleteInsertion/";
  this.urlEditStatusInsertion="http://localhost:8080/insertions/editStatus/";
  this.urlToOrderbyProceA="http://localhost:8080/insertions/getByPriceOrderA";
  this.urlToOrderbyPriceD="http://localhost:8080/insertions/getByPriceOrderD";
  this.urlToOrderByDateDecrescente="http://localhost:8080/insertions/getByDateOrderD";
  this.urlToOrderByDateCrescente="http://localhost:8080/insertions/getByDateOrderA";
  this.utlToEditInsertion="https://localhost:8080/insertions/edit/"
  }



  public getOrderbyPriceD(): Observable<Insertion[]>{
    return this.http.get<Insertion[]>(this.urlToOrderbyPriceD);
  }

  public getOrderbyPriceA(): Observable<Insertion[]>{
    return this.http.get<Insertion[]>(this.urlToOrderbyProceA);
  }

  public getOrderbyDateD(): Observable<Insertion[]>{
    return this.http.get<Insertion[]>(this.urlToOrderByDateDecrescente);
  }

  public getOrderbyDateA(): Observable<Insertion[]>{
    return this.http.get<Insertion[]>(this.urlToOrderByDateCrescente);
  }

  //?published=false&idInsertion=1
  public editStatus(idInsertion: number, pub: boolean): Observable<any>{
    //var httpParams = new HttpParams().append('published', pub.toString()).append('idInsertion', idInsertion.toString());
    //var url=  '?published=' +pub+'&idInsertion='+idInsertion;
    //this.httpParam.set('published', pub.toString());
    //this.httpParam.set('idInsertion', idInsertion.toString());
    //{'published': pub, 'idInsertion':idInsertion}
    //console.log(url);
    return this.http.put('http://localhost:8080/insertions/editStatus/?published='+pub+'&idInsertion='+idInsertion,{}).pipe(
      retry(1)
    );
  }

  public editInsertion(insertion: FormGroup,idInsertion: number){
    return this.http.put(this.utlToEditInsertion+idInsertion, JSON.stringify(insertion.getRawValue()))
  }

  public getActiveInsertions(): Observable<Insertion[]> {

    return this.http.get<Insertion[]>(this.insertionUrl1);

  }

  public getInactiveInsertions():Observable<Insertion[]>{
    return this.http.get<Insertion[]>(this.insertionUrl2);
  }

  public getInsertionsByNameAndCity(title: string, city: string): Observable<any> {
    return this.http.get(this.insertionUrlbynameandcity + '/?name=' + title + '&city=' + city)
  }

  public getInsertionsByNameAndCourse(course: string, title: string): Observable<any> {
    return this.http.get(this.insertionUrlbycourseandname + '/?name=' + title + '&course=' + course)
  }

  public getInsertionById(id: number): Observable<any> {
    return this.http.get(this.insertionUrlbyId + '/' + id);
  }

  public getInsertionBySeller(idUser: number): Observable<any>{
    return this.http.get(this.insertionUrlBySeller + idUser);
  }

  public getActiveInsertionsByCity(city: string): Observable<any> {
    return this.http.get(this.insertionUrlbycity + '/' + city);
  }

  public getActiveInsertionsByCourse(course: string): Observable<any> {
    return this.http.get(this.insertionUrlbycourse + '/' + course);
  }

  public getActiveInsertionsByName(title: string): Observable<any> {
    return this.http.get(this.insertionUrlbyname + '/?name=' + title);
  }

  public getActiveInsertionsByCourseAndCity(course: string, city: string): Observable<any> {
    return this.http.get(this.insertionUrlbycourseandcity + '/?course=' + course + '&city=' + city);
  }

  public getInsertionsByNameCourseCity(course: string, city: string, title: string): Observable<any> {
    return this.http.get(this.insertionUrlbynamecourseandcity + '/?name=' + title + '&course=' + course + '&city=' + city);
  }

  public saveInsertion(insertion: FormGroup): Observable<Insertion> {
    return this.http.post<Insertion>("http://localhost:8080/insertions/addInsertion", JSON.stringify(insertion.getRawValue())).pipe(
      retry(1)
    );
  }

  public savePurchaseInsertion(insertion: Insertion): Observable<Insertion> {
    return this.http.post<Insertion>("http://localhost:8080/insertions/addInsertion", JSON.stringify(insertion)).pipe(
      retry(1)
    );
  }

  public deleteInsertions(idInsertion: number){
    return this.http.delete(this.deleteInsertion + idInsertion).subscribe( result=>{
      console.log(result)
      }
    );
  }

  public getImage(id: number): Observable<Blob>{
    var object=this.http.get(this.getImageUrl+id, {responseType: "blob"});
    return object ;
  }

  imageToShow: any;
  imagesToShow= new Map();
  public getImages(insertions: Insertion[]){
    var i=0;
    for(i=0;i<insertions.length;i++){
      this.getImg(insertions[i].idInsertion);
    }
    return this.imagesToShow;
  }

  getImg(id: number){
    this.getImage(id).subscribe(data => {
      this.createImageFromBlob(data, id);
    }, error => {
      console.log(error);
    });
    window.sessionStorage.removeItem('image');
  }

  createImageFromBlob(image: Blob, id: number) {
    let reader = new FileReader();
    reader.addEventListener("load", () => {
      this.imageToShow = reader.result;
      this.imagesToShow.set(id,this.imageToShow);
    }, false);

    if (image) {
      reader.readAsDataURL(image);
    }
  }

  public uploadImage(formData: FormData) {
    var idInsertion = this.getIdInsertions();
    window.sessionStorage.removeItem('image');
    window.sessionStorage.setItem("image", "yes");
    return this.http.post(this.addImageUrl + idInsertion, formData, {
      reportProgress: true,
      observe: "events"
    })
      .pipe(retry(1)
      );
      //route.na
  }

  public setIdInsertions(id: any) {
    this.idInsertion = id;
  }

  public getIdInsertions() {
    return this.idInsertion;
  }

  /*
  pushFileToStorage(file: File): Observable<HttpEvent<{}>> {
    var idInsertion = this.getIdInsertions();
    window.sessionStorage.removeItem('image');
    window.sessionStorage.setItem("image", "yes");
    const data: FormData = new FormData();
    data.append('file', file);
    return this.http.post(this.addImageUrl + idInsertion, file, {
      reportProgress: true,
      observe: "events"
    })
  }*/
}

