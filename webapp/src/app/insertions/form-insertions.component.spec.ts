import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormInsertionsComponent } from './form-insertions.component';

describe('InsertionsComponent', () => {
  let component: FormInsertionsComponent;
  let fixture: ComponentFixture<FormInsertionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormInsertionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormInsertionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
