import { TestBed } from '@angular/core/testing';

import { InsertionServiceService } from './insertions.service';

describe('InsertionsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InsertionServiceService = TestBed.get(InsertionServiceService);
    expect(service).toBeTruthy();
  });
});
