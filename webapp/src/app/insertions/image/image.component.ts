import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {InsertionServiceService} from "../insertions.service";

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.css']
})
export class ImageComponent implements OnInit {

  imageToShow: any;


  constructor(private insertionService: InsertionServiceService,private httpClient: HttpClient) { }

  ngOnInit() {
  }



}
