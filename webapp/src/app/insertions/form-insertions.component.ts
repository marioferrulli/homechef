import {Component, Input, OnInit} from '@angular/core';
import {Insertion} from "./insertion";

import {Router} from "@angular/router";
import {InsertionDetailsComponent} from "./insertion-details/insertion-details.component";
import {InsertionServiceService} from "./insertions.service";
import {ImageComponent} from "./image/image.component";
import {Image} from "./image";
import {map} from "rxjs/operators";
import {FormBuilder} from "@angular/forms";

@Component({
  selector: 'app-forminsertions',
  templateUrl: './form-insertions.component.html',
  styleUrls: ['./form-insertions.component.css']
})
export class FormInsertionsComponent implements OnInit {

    @Input()insertions: Insertion[];

    searchForm=this.formBuild.group({
      course:[''],
      city:[''],
      title:['']
    })

    course: string ;
    city: string;
    title: string;
    id: string;
    insertion: Insertion;
    @Input() imagestoShow = new Map();
    imageToShow:any;
    image: Image;
    i: number=0;
    servings: string;

    constructor(private insertionService: InsertionServiceService, private route: Router, private formBuild: FormBuilder) { }

    ngOnInit() {

    }

  search(){
    this.course=this.searchForm.get('course').value;
    this.title=this.searchForm.get('title').value;
    this.city=this.searchForm.get('city').value;
      if(this.course=='all' && this.title=="" && this.city!="") {
        this.insertionService.getActiveInsertionsByCity(this.city.toLowerCase()).subscribe(data => {
          this.imagestoShow=this.insertionService.getImages(data);
          this.insertions = data;
        })
      }else if(this.course=='all' && this.title=="" && this.city==""){
        this.insertionService.getActiveInsertions().subscribe(data => {
          this.imagestoShow=this.insertionService.getImages(data);
          this.insertions = data;});
      }else if(this.course != "" && this.title=="" && this.city != "") {
        this.insertionService.getActiveInsertionsByCourseAndCity(this.course, this.city).subscribe(data => {
          this.imagestoShow=this.insertionService.getImages(data);
          this.insertions = data;
        });
      }else if(this.course!="" && this.title!="" && this.city!= ""){
        this.insertionService.getInsertionsByNameCourseCity(this.course,this.city,this.title).subscribe(data=> {
          this.imagestoShow=this.insertionService.getImages(data);
          this.insertions=data;
        });
      }else if(this.course!="" && this.title!="" && this.city==""){
        this.insertionService.getInsertionsByNameAndCourse(this.course,this.title).subscribe(data=> {
          this.imagestoShow=this.insertionService.getImages(data);
          this.insertions=data;
        });
      }else if(this.course!="" && this.title=="" && this.city==""){
        this.insertionService.getActiveInsertionsByCourse(this.course).subscribe(data =>{
          this.imagestoShow=this.insertionService.getImages(data);
          this.insertions=data;
        });
      }else if(this.course=="" && this.title!="" && this.city==""){
        this.insertionService.getActiveInsertionsByName(this.title).subscribe(data =>{
          this.imagestoShow=this.insertionService.getImages(data);
          this.insertions=data;
        });
      }else if(this.course=="" && this.title!="" && this.city!=""){
        this.insertionService.getInsertionsByNameAndCity(this.title, this.city).subscribe(data =>{
          this.imagestoShow=this.insertionService.getImages(data);
          this.insertions=data;
        });
      }else if(this.course=="" && this.title=="" && this.city!=""){
        this.insertionService.getActiveInsertionsByCity(this.city).subscribe(data=>{
          this.imagestoShow=this.insertionService.getImages(data);
          this.insertions=data;
        })
      }
    }




  }

