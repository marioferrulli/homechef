import { Component, OnInit } from '@angular/core';
import {Insertion} from "./insertion";

import {Router} from "@angular/router";
import {InsertionDetailsComponent} from "./insertion-details/insertion-details.component";
import {InsertionServiceService} from "./insertions.service";

@Component({
  selector: 'app-insertions',
  templateUrl: './insertions.component.html',
  styleUrls: ['./insertions.component.css']
})
export class InsertionsComponent implements OnInit {

    insertions: Insertion[];
    course: string;
    city: string;
    title: string;
    id: string;
    insertion: Insertion;
    insertionDetails: InsertionDetailsComponent;
    constructor(private insertionService: InsertionServiceService, private route: Router) { }

    ngOnInit() {
      this.insertionService.getActiveInsertions().subscribe(data => {
        this.insertions = data;
      });
    }

    search(){
      if(this.course=='all' && this.title==null && this.city!=null) {
        this.insertionService.getActiveInsertionsByCity(this.city.toLowerCase()).subscribe(data => {
          this.insertions = data;
        })
      }else if(this.course=='all' && this.title==null && this.city==null){
        this.insertionService.getActiveInsertions().subscribe(data => {
          this.insertions = data;});
      }else if(this.course != null && this.title==null && this.city != null) {
        this.insertionService.getActiveInsertionsByCourseAndCity(this.course, this.city).subscribe(data => {
          this.insertions = data;
        });
      }else if(this.course!=null && this.title!=null && this.city!= null){
        this.insertionService.getInsertionsByNameCourseCity(this.course,this.city,this.title).subscribe(data=> {
          this.insertions=data;
        });
      }else if(this.course!=null && this.title!=null && this.city==null){
        this.insertionService.getInsertionsByNameAndCourse(this.course,this.title).subscribe(data=> {
          this.insertions=data;
        });
      }else if(this.course!=null && this.title==null && this.city==null){
        this.insertionService.getActiveInsertionsByCourse(this.course).subscribe(data =>{
          this.insertions=data;
        });
      }else if(this.course==null && this.title!=null && this.city==null){
        this.insertionService.getActiveInsertionsByName(this.title).subscribe(data =>{
          this.insertions=data;
        });
      }else if(this.course==null && this.title!=null && this.city!=null){
        this.insertionService.getInsertionsByNameAndCity(this.title, this.city).subscribe(data =>{
          this.insertions=data;
        });
      }

    }

    showElement(idInsertion: number){
      this.insertionService.setIdInsertions(idInsertion);
    }


  }

