import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {InsertionServiceService} from "../insertions.service";
import {ActivatedRoute, Router} from "@angular/router";
import {FormBuilder} from "@angular/forms";

@Component({
  selector: 'app-upload-image',
  templateUrl: './upload-image.component.html',
  styleUrls: ['./upload-image.component.css']
})
export class UploadImageComponent implements OnInit {

  fileData: File = null;
  previewUrl:any = null;
  fileUploadProgress: string = null;
  uploadedFilePath: string = null;
  addImageUrl: string;


  constructor(private insertionService: InsertionServiceService,private activateRoute: ActivatedRoute, private route: Router,private formBuilder: FormBuilder) {

  }

  ngOnInit() {
  }

  fileProgress(fileInput: any) {
    this.fileData = <File>fileInput.target.files[0];
    this.preview();
  }

  preview() {
    // Show preview
    var mimeType = this.fileData.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    var reader = new FileReader();
    reader.readAsDataURL(this.fileData);
    reader.onload = (_event) => {
      this.previewUrl = reader.result;
    }
  }

  onSubmit() {
    const formData = new FormData();
    formData.append('file', this.fileData);
    this.insertionService.uploadImage(formData).subscribe(res => {
      console.log(res);
      window.sessionStorage.setItem("image",res[0])
      var insertionId: number = this.activateRoute.snapshot.params['insertionId'];
      window.location.replace('/insertions/'+insertionId);
    });
  }
}

