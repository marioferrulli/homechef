import {User} from "../user/user";

export class Insertion{
  idInsertion: number;
  title: string;
  public user: User;
  date: Date;
  city: string;
  text: string;
  servings: number;
  expiration: string;
  course: string;
  ingredients: string[];
  price: number;
  description: string;
  published: boolean;
}
