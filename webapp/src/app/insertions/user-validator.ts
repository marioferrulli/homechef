import {AbstractControl, ValidationErrors} from "@angular/forms";
import {UserService} from "../user/user-service.service";

export class UserValidator {
  static userExist(control: AbstractControl): ValidationErrors | null{
    if(control.value as number){
      var userService: UserService;
      userService.findUserById(control.value).subscribe(result =>
        {
          if (result!=null){
            return {userExist: true}
          }
        }
      )
    }
    return null;
  }
}
