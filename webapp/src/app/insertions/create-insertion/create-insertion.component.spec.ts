import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateInsertionComponent } from './create-insertion.component';

describe('CreateInsertionComponent', () => {
  let component: CreateInsertionComponent;
  let fixture: ComponentFixture<CreateInsertionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateInsertionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateInsertionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
