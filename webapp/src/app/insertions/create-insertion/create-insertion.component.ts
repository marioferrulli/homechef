import { Component, OnInit } from '@angular/core';
import {Insertion} from "../insertion";
import {UserService} from "../../user/user-service.service";
import {SocialUser} from "angularx-social-login";
import {User} from "../../user/user";
import {Observable} from "rxjs";
import {InsertionServiceService} from "../insertions.service";
import {FormBuilder, FormGroup, FormControl, FormArray} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
  selector: 'app-create-insertion',
  templateUrl: './create-insertion.component.html',
  styleUrls: ['./create-insertion.component.css'],
  providers: [UserService]
})
export class CreateInsertionComponent implements OnInit {

  city: string;
  title: string;
  course: string;
  socialuser: SocialUser=JSON.parse(localStorage.getItem('socialusers'));
  idUser = parseFloat(JSON.parse(localStorage.getItem('userId')));
  users : User[];
  idCurrentUser: number;

  newInsertion = this.formBuilder.group({
    title: [''],
    user: [''],
    city: [''],
    servings:[''],
    price: [''],
    ingredients: this.formBuilder.array([
      this.formBuilder.control('')
      ]
    ),
    description:[''],
    expiration: [''],
    course: [''],
    published: [true]
  });



  constructor(private formBuilder: FormBuilder,private insertionService: InsertionServiceService, private currentUser: UserService, private route: Router) {

  }

  ngOnInit() {
    //this.save();
    this.getIdUser();
  }

  getIdUser(){
   return this.currentUser.findUserById(this.idUser).subscribe(data=>
    {
      this.users=data;
      this.idCurrentUser=this.users[0].idUser;
      console.log(this.newInsertion.value)
    });
  }

  get ingredients() {
    return this.newInsertion.get('ingredients') as FormArray;
  }

  addIngredients() {
    this.ingredients.push(this.formBuilder.control(''));
  }
  //+ serve per trasformare una stringa in un number
  save(){
    var exp= new Date();
    if(this.newInsertion.get('expiration').value!=""){
      exp=  this.newInsertion.get('expiration').value;
      var data= exp.getDate()+'/'+(exp.getMonth()+1)+'/'+exp.getFullYear();
      this.newInsertion.patchValue({
        expiration: data
      })
    }

    this.newInsertion.patchValue({
      price: parseFloat(this.newInsertion.get('price').value)
    });
    this.newInsertion.patchValue({
      servings: parseFloat(this.newInsertion.get('servings').value)
    })
    this.newInsertion.patchValue({
      user: { idUser: this.idCurrentUser}
    })
    console.log(JSON.stringify(this.newInsertion.getRawValue()));
    var insertion: Insertion;
    insertion = this.newInsertion.getRawValue();
    var idInsertion
    //this.currentUser.getCurrentUser(this.idUser).subscribe(result =>{
     // newInsertion.user=result;
    //});
    this.insertionService.saveInsertion(this.newInsertion).subscribe(result =>{
      idInsertion=result;
      this.insertionService.setIdInsertions(idInsertion);
      this.route.navigate(['/uploadImage/'+idInsertion]);
    });


  }

  createInsertionWithImage(idInsertion){

  }
}

