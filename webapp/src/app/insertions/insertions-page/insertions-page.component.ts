import {Component, Input, OnInit} from '@angular/core';
import {Insertion} from "../insertion";
import {ActivatedRoute} from "@angular/router";
import {InsertionServiceService} from "../insertions.service";

@Component({
  selector: 'app-insertions-page',
  templateUrl: './insertions-page.component.html',
  styleUrls: ['./insertions-page.component.css']
})
export class InsertionsPageComponent implements OnInit {

  constructor(private route: ActivatedRoute, private insertionService: InsertionServiceService) { }

  @Input() insertions: Insertion[];
  imagestoShow= new Map();
  city: string;
  params: string;

  ngOnInit(): void {
    console.log(this.route.url)

    this.params=this.route.snapshot.params['argument'];

    if(this.params=='priceA'){
      this.giveInsertionOrderPriceA()
    }else if(this.params=='dateD'){
      this.giveInsertionOrderDateD()
    }


    switch (this.params) {
      case 'priceA':
        this.giveInsertionOrderPriceA();
        break;
      case 'priceD':
        this.giveInsertionOrderPriceD();
        break;
      case 'dateA':
        this.giveInsertionOrderDateA();
        break;
      case 'dateD':
        this.giveInsertionOrderDateD();
        break;
      case 'insertions':
        this.giveActionInsertions();
        break;
      default:
        alert("No such day exists!");
        break;
    }
  }

  giveActionInsertions(){
      this.insertionService.getActiveInsertions().subscribe(data => {
        this.imagestoShow=this.insertionService.getImages(data);
        this.insertions=data;
      })
  }

  giveInsertionOrderPriceA() {
    this.insertionService.getOrderbyPriceA().subscribe(result => {
      this.imagestoShow = this.insertionService.getImages(result);
      this.insertions = result;
    });
  }

  giveInsertionOrderPriceD() {
    this.insertionService.getOrderbyPriceD().subscribe(result => {
      this.imagestoShow = this.insertionService.getImages(result);
      this.insertions = result;
    })
  }

  giveInsertionOrderDateA() {
    this.insertionService.getOrderbyDateA().subscribe(result => {
      this.imagestoShow = this.insertionService.getImages(result);
      this.insertions = result;
    })
  }

  giveInsertionOrderDateD() {
    this.insertionService.getOrderbyDateD().subscribe(result => {
      this.imagestoShow = this.insertionService.getImages(result);
      this.insertions = result;
    })
  }

  giveInsertionCity(city: string){
    this.city=city;
  }
}
