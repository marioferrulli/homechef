import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InsertionsPageComponent } from './insertions-page.component';

describe('InsertionsPageComponent', () => {
  let component: InsertionsPageComponent;
  let fixture: ComponentFixture<InsertionsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InsertionsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsertionsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
