import {Component, Input, OnInit} from '@angular/core';
import {Image} from "../image";
import {Insertion} from "../insertion";

@Component({
  selector: 'app-vetrina',
  templateUrl: './shop-window.component.html',
  styleUrls: ['./shop-window.component.css']
})
export class ShopWindowComponent implements OnInit {

  constructor() { }

  @Input() imagestoShow= new Map();
  @Input() insertions: Insertion[];

  ngOnInit(): void {
  }

}
