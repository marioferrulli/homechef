import { Component, OnInit } from '@angular/core';
import {Insertion} from "../insertion";
import {InsertionServiceService} from "../insertions.service";
import {Purchase} from "../../purchase/purchase";
import {FormBuilder, Validators} from "@angular/forms";
import {PurchaseService} from "../../purchase/purchase.service";
import {User} from "../../user/user";
import {ActivatedRoute, Router} from "@angular/router";
import {InsertionDetailsComponent} from "../insertion-details/insertion-details.component";
import {UserValidator} from "../user-validator";
import {UserService} from "../../user/user-service.service";
import {SocialUser} from "angularx-social-login";

@Component({
  selector: 'app-insertions-by-id',
  templateUrl: './insertions-by-id.component.html',
  styleUrls: ['./insertions-by-id.component.css'],
  providers:[ InsertionServiceService, PurchaseService]
})
export class InsertionsByIdComponent implements OnInit {
  insertions: Insertion[];
  imageToShow: any;
  idInsertion: number;
  insertion: Insertion;
  idBuyerUser: number;
  newPurchase = this.formBuilder.group({
    user: this.formBuilder.group({
      idUser: [''],
    }),
    insertion: ['']
  });
  socialuser: SocialUser= JSON.parse(localStorage.getItem('socialusers'));

  constructor(private routerActivate: ActivatedRoute,private formBuilder: FormBuilder, private route: Router, private currentUser: UserService, private purchaseService: PurchaseService,private insertionsService: InsertionServiceService) { }
  imagesToShow= new Map();
  datas: Insertion[];
  ngOnInit(): void {
    //var idUs= parseFloat(JSON.parse(localStorage.getItem('userId')));
    var idUs= this.routerActivate.snapshot.params['userId'];
    this.insertionsService.getInsertionBySeller(idUs).subscribe(data => {
      this.imagesToShow=this.insertionsService.getImages(data);
      var i;
      this.datas=data;
      for (i = 0; i < this.datas.length; i++) {
        if(this.datas[i].published==false){
          this.addPurchases(this.datas[i].idInsertion);
        }
      }
      this.insertions = data;
    })
  }

  purchase: any;
  addPurchases(idInsertion: number){
    this.purchaseService.findBuyerByIdInsertion(idInsertion).subscribe(result=>{
      this.purchase=result;
      console.log(result)
      //this.purchases.push(this.purchase);
    })
  }
  users: any[];
  realUserId: any;
  getIdUser(idUser: number){
    this.currentUser.findUserById(idUser).subscribe(data=>
    {
      this.users=data;
      this.realUserId=this.users[0].idUser;
      this.newPurchase.patchValue({
        user: { idUser: this.realUserId}
      })
    });
  }

  selling(idInsertion: number) {
    this.idInsertion = idInsertion;
    this.idBuyerUser = this.newPurchase.get('user').get('idUser').value;
    this.getIdUser(this.idBuyerUser);
    this.newPurchase.patchValue({
      insertion: {idInsertion: this.idInsertion}
    })
    // this.newPurchase.setValidators(UserValidator.userExist);
    console.log(JSON.stringify(this.newPurchase.getRawValue()));
    var idPurchase: any = null;
    this.purchaseService.savePurchase(this.newPurchase).subscribe(result => {
      idPurchase = result;
      if (idPurchase != null) {
        this.updateInsertion(idInsertion);
        //this.generateEmail();
      }

    })
  }

  updateInsertion(idInsertion: number){
    this.insertionsService.getInsertionById(this.idInsertion).subscribe(result=>
    {
      this.imagesToShow=this.insertionsService.getImages(result);
      this.insertions = result;
      this.insertion=this.insertions[0];
      this.insertionsService.editStatus(this.insertion.idInsertion, false).subscribe(result =>
        console.log(result[0])
      )
      this.route.navigate(['/insertions/'+this.insertion.idInsertion]);
    })
  }

  delete(idInsertions:number){
    this.insertionsService.deleteInsertions(idInsertions);
    this.ngOnInit();
  }
}
