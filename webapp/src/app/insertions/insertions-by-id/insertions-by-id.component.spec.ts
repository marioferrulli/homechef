import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InsertionsByIdComponent } from './insertions-by-id.component';

describe('InsertionsByIdComponent', () => {
  let component: InsertionsByIdComponent;
  let fixture: ComponentFixture<InsertionsByIdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InsertionsByIdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsertionsByIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
