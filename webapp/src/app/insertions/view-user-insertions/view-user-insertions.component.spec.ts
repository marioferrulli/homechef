import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewUserInsertionsComponent } from './view-user-insertions.component';

describe('ViewUserInsertionsComponent', () => {
  let component: ViewUserInsertionsComponent;
  let fixture: ComponentFixture<ViewUserInsertionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewUserInsertionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewUserInsertionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
