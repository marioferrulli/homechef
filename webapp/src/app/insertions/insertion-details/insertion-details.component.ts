import { Component, OnInit } from '@angular/core';
import {InsertionServiceService} from "../insertions.service";
import {Insertion} from "../insertion";
import {ActivatedRoute, Router} from "@angular/router";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import {FormBuilder} from "@angular/forms";
import {SocialUser} from "angularx-social-login";
import {UserService} from "../../user/user-service.service";
import {PurchaseService} from "../../purchase/purchase.service";
import {User} from "../../user/user";
import {ReviewResponse} from "../../review/review-response";
import {ReviewService} from "../../review/review.service";

@Component({
  selector: 'app-insertion-details',
  templateUrl: './insertion-details.component.html',
  styleUrls: ['./insertion-details.component.css'],
  providers:[ InsertionServiceService]
})
export class InsertionDetailsComponent implements OnInit {

  idInsertion: number;
  idBuyerUser: number;
  imagesToShow= new Map();
  data: Insertion[];
  insertion: Insertion;
  userId: number = parseFloat(JSON.parse(localStorage.getItem('userId')));
  socialuser: SocialUser= JSON.parse(localStorage.getItem('socialusers'));
  newPurchase = this.formBuilder.group({
    user: this.formBuilder.group({
      idUser: [''],
    }),
    insertion: ['']
  });



  id: number;
  constructor(private reviewService: ReviewService, private purchaseService: PurchaseService,private userService: UserService,private formBuilder: FormBuilder,private insertionService: InsertionServiceService,private route: ActivatedRoute, private router: Router) {
    //this.id= route.params.pipe(map(p=>p.id));
  }

  ngOnInit(): void {
      this.showElement();
  }

  userPurchase: User;
  showElement(){
    this.id=this.route.snapshot.params['insertionId'];
    console.log(this.id)
    this.insertionService.getInsertionById(this.id).subscribe(data => {
      this.data=data;
      this.imagesToShow=this.insertionService.getImages(this.data);
      this.insertion = data[0];
      this.idInsertion=this.insertion.idInsertion;

      if(this.insertion.published==false){
        this.purchaseService.findBuyerByIdInsertion(this.insertion.idInsertion).subscribe(
          result=>{
            this.userService.findUserById(parseInt(result.toString())).subscribe(user=>
            {
              this.userPurchase=user[0];
              this.findreview(this.userPurchase.idUser)
            })
        });

      }
      //this.src="../src/main/resources/img/insertion"+this.id;
      });

  }

  idReview: number;
  reviewBool: boolean=false;
  findreview(idUser: number){
    this.reviewService.findReviewByUserAndInsertion(idUser,this.idInsertion).subscribe(result=>{
      this.idReview=result[0];
      //(this.idReview!=undefined){
        this.reviewBool=true;
      //}
    })
  }

  addReview(){
    this.router.navigate(['purchases/addReview/'+this.idInsertion])
  }

  goToReview(){
    this.reviewService.findReviewByUserAndInsertion(this.userPurchase.idUser,this.idInsertion).subscribe(result=>{
      this.idReview=result;

      if(this.userId==this.userPurchase.idUser){
        this.router.navigate(['reviews/viewAReview/'+this.idReview])
      }
      else{
      this.router.navigate(['responses/addReviewResponse/'+this.idReview])}

      if(this.idReview!=null){
        this.reviewBool=true;
      }
    })

  }

  goToChat(idReceiver: number){
    window.location.replace('chat/viewChats/'+idReceiver);
  }

  users: any[];
  realUserId: any;
  getIdUser(idUser: number){
    this.userService.findUserById(idUser).subscribe(data=>
    {
      this.users=data;
      this.realUserId=this.users[0].idUser;
      this.newPurchase.patchValue({
        user: { idUser: this.realUserId}
      })
    });
  }
  idPurchase: any;
  selling(idInsertion: number) {
    this.idInsertion = idInsertion;
    this.idBuyerUser = this.newPurchase.get('user').get('idUser').value;
    this.getIdUser(this.idBuyerUser);
    this.newPurchase.patchValue({
      insertion: {idInsertion: this.idInsertion}
    })
    // this.newPurchase.setValidators(UserValidator.userExist);
    console.log(JSON.stringify(this.newPurchase.getRawValue()));

    this.purchaseService.savePurchase(this.newPurchase).subscribe(result => {
      console.log(result)
      this.idPurchase = result;
      if (this.idPurchase != null) {
        this.updateInsertion(idInsertion);
        //this.generateEmail();
      }

    })
  }

  insertions: Insertion[];

  updateInsertion(idInsertion: number){
    this.insertionService.getInsertionById(this.idInsertion).subscribe(result=>
    {
      this.imagesToShow=this.insertionService.getImages(result);
      this.insertions = result;
      this.insertion=this.insertions[0];
      this.insertionService.editStatus(this.insertion.idInsertion, false).subscribe(result =>
        console.log(result[0])
      )
      window.location.replace('/insertions/'+this.insertion.idInsertion);
    })
  }

  delete(idInsertions:number){
    this.insertionService.deleteInsertions(idInsertions);
    window.location.replace('/insertions/profile/'+this.userId);
  }



}
