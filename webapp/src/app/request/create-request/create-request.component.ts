import { Component, OnInit } from '@angular/core';
import {User} from "../../user/user";
import {FormArray, FormBuilder} from "@angular/forms";
import {InsertionServiceService} from "../../insertions/insertions.service";
import {UserService} from "../../user/user-service.service";
import {Insertion} from "../../insertions/insertion";
import {RequestService} from "../request.service";
import {Router} from "@angular/router";
import {SocialUser} from "angularx-social-login";
import {Request} from "../request";

@Component({
  selector: 'app-create-request',
  templateUrl: './create-request.component.html',
  styleUrls: ['./create-request.component.css']
})
export class CreateRequestComponent implements OnInit {

  city: string;
  title: string;
  course: string;
  user: User;

  data: Date;
  minDate: Date;
  socialuser: SocialUser=JSON.parse(localStorage.getItem('socialusers'));
  idUser = parseFloat(JSON.parse(localStorage.getItem('userId')));

  newRequest = this.formBuilder.group({
    title: [''],
    user: [''],
    text: [''],
    city: [''],
    servings: [''],
    expiration: [''],
    course: [''],
    active: [true]
  });

  constructor(private formBuilder: FormBuilder,private requestService: RequestService, private route: Router, private currentUser: UserService) {

  }

  ngOnInit() {
    //this.save();
    this.getIdUser();
  }

  //+ serve per trasformare una stringa in un number
  save(){
    var exp= new Date();

    if ( this.newRequest.get('expiration').value!=""){
      exp= this.newRequest.get('expiration').value;
      var data= exp.getDate()+'/'+(exp.getMonth()+1)+'/'+exp.getFullYear();
      this.newRequest.patchValue({
        expiration: data
      })
    }

    this.newRequest.patchValue({
      servings: parseFloat(this.newRequest.get('servings').value)
    })
    this.newRequest.patchValue({
      user: { idUser: this.idCurrentUser}
    })

    console.log(JSON.stringify(this.newRequest.getRawValue()));
    var request= new Request();
    request = this.newRequest.getRawValue();
    var idRequest;
    this.requestService.saveRequest(this.newRequest).subscribe(result =>{
      idRequest=result;
      console.log(idRequest);
    });
    window.location.replace('requests/viewRequests/menu/requests')
  }

  users: User[];
  idCurrentUser:number;

  getIdUser(){
    return this.currentUser.findUserById(this.idUser).subscribe(data=>
    {
      this.users=data;
      this.idCurrentUser=this.users[0].idUser;
      console.log(this.newRequest.value)
    });
  }
}
