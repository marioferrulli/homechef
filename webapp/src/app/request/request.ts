import {User} from "../user/user";

export class Request {
  idRequest: number;
  title: string;
  public user: User;
  date: Date;
  city: string;
  servings: number;
  expiration: string;
  course: string;
  text: string;
  active: boolean;
}
