import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {RequestService} from "../request.service";
import {Request} from "../request";

@Component({
  selector: 'app-check-page-request',
  templateUrl: './check-page-request.component.html',
  styleUrls: ['./check-page-request.component.css']
})
export class CheckPageRequestComponent implements OnInit {

  constructor(private route: Router, private requestService: RequestService) { }

  ngOnInit() {
  }

  viewActiveRequests(){
    this.route.navigate(["/requests/viewRequests/menu/requests"]);
  }

  insertNewRequest(){
    this.route.navigate(["requests/insertNewRequest"]);
  }


}
