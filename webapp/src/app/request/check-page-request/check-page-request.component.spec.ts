import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckPageRequestComponent } from './check-page-request.component';

describe('CheckPageRequestComponent', () => {
  let component: CheckPageRequestComponent;
  let fixture: ComponentFixture<CheckPageRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckPageRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckPageRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
