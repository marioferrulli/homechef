import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestByIdComponent } from './request-by-id.component';

describe('RequestByIdComponent', () => {
  let component: RequestByIdComponent;
  let fixture: ComponentFixture<RequestByIdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestByIdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestByIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
