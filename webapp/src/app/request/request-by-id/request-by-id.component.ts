import { Component, OnInit } from '@angular/core';
import {RequestService} from "../request.service";
import {Request} from "../request";
import {ActivatedRoute, ActivatedRouteSnapshot} from "@angular/router";

@Component({
  selector: 'app-request-by-id',
  templateUrl: './request-by-id.component.html',
  styleUrls: ['./request-by-id.component.css']
})
export class RequestByIdComponent implements OnInit {

  constructor(private requestService: RequestService, private route: ActivatedRoute) { }
  requests: Request[];
  ngOnInit(): void {
    var userId=this.route.snapshot.params['userId'];
    this.requestService.findBySeller(userId).subscribe(result=>{
      this.requests=result;
    })
  }

  delete(idR:number){
    this.requestService.deleteRequest(idR);
    this.ngOnInit();
  }
}
