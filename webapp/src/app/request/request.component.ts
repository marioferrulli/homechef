import {Component, Input, OnInit} from '@angular/core';
import {Request} from "./request";
import {RequestService} from "./request.service";
import {FormBuilder} from "@angular/forms";

@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.css']
})
export class RequestComponent implements OnInit {

  @Input() requests: Request[];

  searchRequest=this.formBuilder.group({
    course:[''],
    city:[''],
    title:['']
  });

  course: string;
  city: string;
  title: string;
  id: string;
  request: Request;

  constructor(private requestService: RequestService, private formBuilder: FormBuilder) {

  }

  ngOnInit() {

  }

  search(){
    this.course=this.searchRequest.get('course').value;
    this.city=this.searchRequest.get('city').value;
    this.title=this.searchRequest.get('title').value;

    if(this.course=='all' && this.title==undefined && this.city!=undefined) {
      this.requestService.getActiveRequestsByCity(this.city.toLowerCase()).subscribe(data => {
        this.requests = data;
      });
    }else if(this.course=='all' && this.title==undefined && this.city==undefined){
      this.requestService.getActiveRequests().subscribe(data => {
        this.requests = data;
      });
    }else if(this.course != null && this.title==undefined && this.city != undefined) {
      this.requestService.getActiveRequestsByCourseAndCity(this.course, this.city).subscribe(data => {
        this.requests = data;
      });
    }else if(this.course!=null && this.title!=undefined && this.city!=undefined){
      this.requestService.getRequestsByNameCourseCity(this.course,this.city,this.title).subscribe(data=> {
        this.requests=data;
      });
    }else if(this.course!=null && this.title!=undefined && this.city==undefined){
      this.requestService.getRequestsByNameAndCourse(this.course,this.title).subscribe(data=> {
        this.requests=data;
      });
    }else if(this.course!=null && this.title==undefined && this.city==undefined){
      this.requestService.getActiveRequestsByCourse(this.course).subscribe(data =>{
        this.requests=data;
      });
    }else if(this.course==null && this.title!=undefined && this.city==undefined){
      this.requestService.getActiveRequestsByName(this.title).subscribe(data =>{
        this.requests=data;
      });
    }else if(this.course==null && this.title!=undefined && this.city!=undefined){
      this.requestService.getRequestsByNameAndCity(this.title, this.city).subscribe(data =>{
        this.requests=data;
      });
    }

  }

SS

}
