import { Injectable } from '@angular/core';
import {FormGroup} from "@angular/forms";
import {Observable, of} from "rxjs";
import {catchError, map, retry, tap} from "rxjs/operators";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Request} from "./request";
import {Insertion} from "../insertions/insertion";

@Injectable({
  providedIn: 'root'
})
export class

RequestService {

  public requestUrl1: string;
  public requestUrlbyId: string;
  public requestUrlbycity: string;
  public requestUrlbycourseandcity: string;
  public requestUrlbynamecourseandcity: string;
  public requestUrlbycourse: string;
  public requestUrlbynameandcity: string;
  public requestUrlbycourseandname: string;
  public jwt;
  public requestUrlbyname: string;
  public addRequest: string;
  public idRequest: number;
  public requestUrlFindBySeller: string;
  public urlToOrderByDateDecrescente: string;
  public urlToOrderByDateCrescente: string;
  public urlToOrderbyPriceD:string;
  public urlToOrderbyProceA: string;
  public deleteUrl:string;
public utlToseller: string;

  constructor(private http: HttpClient) {
    this.requestUrl1 = "http://localhost:8080/requests/findActiveRequests";
    this.requestUrlbyId = "http://localhost:8080/requests/findById";
    this.requestUrlbycity = "http://localhost:8080/requests/getActiveRequestsByCity";
    this.requestUrlbycourseandcity = "http://localhost:8080/requests/getActiveRequestsByCourseAndCity";
    this.requestUrlbynameandcity = "http://localhost:8080/requests/getActiveRequestsByNameAndCity"
    this.requestUrlbynamecourseandcity = "http://localhost:8080/requests/getActiveRequestsByNameCourseCity";
    this.requestUrlbycourse = "http://localhost:8080/requests/getActiveRequestssByCourse";
    this.requestUrlbycourseandname= "http://localhost:8080/requests/getActionsRequestsByNameAndCourse"
    this.requestUrlbyname= "http://localhost:8080/requests/getActiveRequestsByName";
    this.addRequest = "http://localhost:8080/requests/addRequest";
    this.requestUrlFindBySeller= "http://localhost:8080/requests/findBySeller/";
    this.urlToOrderbyProceA="http://localhost:8080/requests/getByPriceOrderD";
    this.urlToOrderbyPriceD="http://localhost/requests/getByPriceOrderD";
    this.urlToOrderByDateDecrescente="http://localhost:8080/requests/getByDateOrderD";
    this.urlToOrderByDateCrescente="http://localhost:8080/requests/getByDateOrderA";
    this.deleteUrl="http://localhost:8080/requests/deleteRequest";
    this.utlToseller="https://localhost:8080/requests/findBySeller/";
  }

  public getRequestBySeller(idUser: number): Observable<Request[]>{
    return this.http.get<Request[]>(this.utlToseller+idUser);
  }

  public getOrderbyPriceD(): Observable<Request[]>{
    return this.http.get<Request[]>(this.urlToOrderbyPriceD);
  }

  public getOrderbyPriceA(): Observable<Request[]>{
    return this.http.get<Request[]>(this.urlToOrderbyProceA);
  }

  public getOrderbyDateD(): Observable<Request[]>{
    return this.http.get<Request[]>(this.urlToOrderByDateDecrescente);
  }

  public getOrderbyDateA(): Observable<Request[]>{
    return this.http.get<Request[]>(this.urlToOrderByDateCrescente);
  }

  public saveRequest(request: FormGroup): Observable<Request> {
    return this.http.post<Request>("http://localhost:8080/requests/addRequest", request.getRawValue()).pipe(
      retry(1)

    )
  }

  public getActiveRequests(): Observable<Request[]> {
    this.jwt= window.sessionStorage.getItem('jwtToken');
    return this.http.get<Request[]>(this.requestUrl1,  {headers : new HttpHeaders().set('X-Auth-Token', this.jwt)});

  }

  public getRequestsByNameAndCity(title: string, city: string): Observable<any>{
    return this.http.get(this.requestUrlbynameandcity + '/?name='+title + '&city='+city)
  }

  public getRequestsByNameAndCourse(course: string, title: string): Observable<any>{
    return this.http.get(this.requestUrlbycourseandname + '/?title='+title + '&course='+course)
  }

  public getRequestById(id : number): Observable<any>{
    return this.http.get(this.requestUrlbyId + '/' + id);
  }

  public getActiveRequestsByCity(city: string): Observable<any> {
    return this.http.get(this.requestUrlbycity + '/' + city);
  }

  public getActiveRequestsByCourse(course: string): Observable<any>{
    return this.http.get(this.requestUrlbycourse + '/'+course);
  }

  public getActiveRequestsByName(title: string): Observable<any>{
    return this.http.get(this.requestUrlbyname + '/'+title);
  }

  public getActiveRequestsByCourseAndCity(course: string, city: string): Observable<any> {
    return this.http.get(this.requestUrlbycourseandcity + '/?course=' + course + '&city=' + city);
  }

  public getRequestsByNameCourseCity(course: string, city: string, title: string): Observable<any>{
    return this.http.get(this.requestUrlbynamecourseandcity + '/?title='+title + '&course='+course+'&city='+city);
  }

  public setIdRequests(id: number){
    this.idRequest=id;
  }

  public findBySeller(userId: number): Observable<any>{
    return this.http.get<any>(this.requestUrlFindBySeller+userId);
  }


  public deleteRequest(requestId:number){
    return this.http.delete(this.deleteUrl+'/'+requestId).subscribe( result=>{
      console.log(result);})
  }
}
