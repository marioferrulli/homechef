import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {InsertionServiceService} from "../../insertions/insertions.service";
import {Insertion} from "../../insertions/insertion";
import {RequestService} from "../request.service";
import {Request} from "../request";

@Component({
  selector: 'app-request-detail',
  templateUrl: './request-detail.component.html',
  styleUrls: ['./request-detail.component.css']
})
export class RequestDetailComponent implements OnInit {

  constructor(private route: ActivatedRoute, private requestService: RequestService, private router: Router) {
  }

  id: number;
  imagesToShow= new Map();
  request: Request;
  userId: number = parseFloat(JSON.parse(localStorage.getItem('userId')));


  ngOnInit(): void {
    this.showElement();
  }

  delete(idRequest:number){
    this.requestService.deleteRequest(idRequest);
    window.location.replace('requests/viewRequests/'+this.userId);
  }

  showElement(){
    this.id=this.route.snapshot.params['idRequest'];
    console.log(this.id)
    this.requestService.getRequestById(this.id).subscribe(data => {

      this.request = data[0];
    });

  }

  goToChat(idReceiver: number){
    this.router.navigate(['chat/viewChats/'+idReceiver]);
  }

}
