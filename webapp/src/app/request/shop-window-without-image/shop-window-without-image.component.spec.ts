import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopWindowWithoutImageComponent } from './shop-window-without-image.component';

describe('VetrinaWithoutImageComponent', () => {
  let component: ShopWindowWithoutImageComponent;
  let fixture: ComponentFixture<ShopWindowWithoutImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShopWindowWithoutImageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopWindowWithoutImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
