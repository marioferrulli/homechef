import {Component, Input, OnInit} from '@angular/core';
import {Request} from "../request";

@Component({
  selector: 'app-vetrina-without-image',
  templateUrl: './shop-window-without-image.component.html',
  styleUrls: ['./shop-window-without-image.component.css']
})
export class ShopWindowWithoutImageComponent implements OnInit {

  constructor() { }

  @Input() requests: Request[];
  ngOnInit(): void {
  }

}
