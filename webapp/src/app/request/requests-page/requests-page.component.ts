import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {RequestService} from "../request.service";
import {Reexport} from "@angular/compiler-cli/src/ngtsc/imports";
import {Request} from "../request";

@Component({
  selector: 'app-requests-page',
  templateUrl: './requests-page.component.html',
  styleUrls: ['./requests-page.component.css']
})
export class RequestsPageComponent implements OnInit {

  constructor(private route: ActivatedRoute, private requestService: RequestService) { }

  ngOnInit(): void {
    switch (this.route.snapshot.params['argument']) {
      case 'priceA':
        this.giveRequestsOrderPriceA();
        break;
      case 'priceD':
        this.giveRequestsOrderPriceD();
        break;
      case 'dateA':
        this.giveRequestsOrderDateA();
        break;
      case 'dateD':
        this.giveRequestsOrderDateD();
        break;
      case 'requests':
        this.giveActiveRequests();
        break;
      default:
        alert("No such day exists!");
        break;
    }
  }

  requests: Request[];
  giveActiveRequests(){
    this.requestService.getActiveRequests().subscribe(data => {
      this.requests = data;
    });
  }

  giveRequestsOrderPriceA() {
    this.requestService.getOrderbyPriceA().subscribe(result => {
      this.requests = result;
    });
  }

  giveRequestsOrderPriceD() {
    this.requestService.getOrderbyPriceD().subscribe(result => {
      this.requests = result;
    })
  }

  giveRequestsOrderDateA() {
    this.requestService.getOrderbyDateA().subscribe(result => {
      this.requests = result;
    })
  }

  giveRequestsOrderDateD() {
    this.requestService.getOrderbyDateD().subscribe(result => {
      this.requests = result;
    })
  }


}
