import { Component, OnInit } from '@angular/core';
import {UserService} from "../user/user-service.service";
import {Router} from "@angular/router";
import {AuthService, SocialUser} from "angularx-social-login";
import {User} from "../user/user";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  socialusers:any;
  constructor( private userService: UserService,  private router: Router) { }
  ngOnInit() {
    this.socialusers = JSON.parse(localStorage.getItem('socialusers'));
    console.log(this.socialusers.image);
  }
  logout() {
    alert(1);
    this.userService.fbLogout();
  }

}
