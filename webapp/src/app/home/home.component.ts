import { Component, OnInit } from '@angular/core';

import {Router} from "@angular/router";
import {UserService} from "../user/user-service.service";


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {



  constructor(private route: Router, private userService: UserService) {
  }

  ngOnInit() {

  }

  addInsertion(){
    this.route.navigate(['/addInsertion']);
  }

  viewInsertions(){
    this.route.navigate(['/insertions/viewInsertions/insertions']);
  }

  viewRequests(){
    this.route.navigate(['/requests']);
  }
}
