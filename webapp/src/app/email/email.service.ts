import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Email} from "./email";
import {retry} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class EmailService {

  public urlSendEmail: string;

  constructor(private http: HttpClient) {
    this.urlSendEmail="http://localhost:8080/send-mail"
  }

  sendEmail(email: Email){
    return this.http.request(this.urlSendEmail,JSON.stringify(email)).pipe(
      retry(1)
    );
  }
}
