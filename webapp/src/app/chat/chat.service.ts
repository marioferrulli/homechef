import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable, pipe} from "rxjs";
import {Chat} from "./chat";
import {FormGroup} from "@angular/forms";
import {retry} from "rxjs/operators";
import {Conversations} from "./conversations";

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  public urlAddMessageandgetConv: string;
  public urlGetConversation: string;
  public urlGetAllConversation:string;
  public urlAddMessage: string;

  constructor(private http: HttpClient) {
    this.urlAddMessageandgetConv= "http://localhost:8080/messages/addMessageAndGetConversation";
    this.urlGetConversation="http://localhost:8080/messages/getConversation/?user1=";
    this.urlGetAllConversation="http://localhost:8080/messages/getAllConversations/";
    this.urlAddMessage="http://localhost:8080/messages/addMessage";
  }



  addMessageAndGetConversation(message: FormGroup): Observable<Chat>{
    return this.http.post<Chat>(this.urlAddMessageandgetConv,JSON.stringify(message.getRawValue())).pipe(
      retry(1)
    );
  }

  getConversation(user1:number, user2:number): Observable<Chat>{
    return this.http.get<Chat>(this.urlGetConversation+user1+'&user2='+user2);
  }

  getAllConversation(user: number): Observable<Conversations>{
    return this.http.get<Conversations>(this.urlGetAllConversation+user);
  }


}
