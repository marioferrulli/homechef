import {Component, Input, OnInit} from '@angular/core';
import {Message} from "../message";
import {ActivatedRoute} from "@angular/router";
;

@Component({
  selector: 'app-chat-view',
  templateUrl: './chat-view.component.html',
  styleUrls: ['./chat-view.component.css']
})
export class ChatViewComponent implements OnInit {

  constructor(private route: ActivatedRoute) { }

  idReceiver: number;
  idSender: number;

  ngOnInit(): void {
    this.idSender=this.route.snapshot.params['user1'];

  }


}
