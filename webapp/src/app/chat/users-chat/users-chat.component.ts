import {Component, Input, OnInit} from '@angular/core';
import {Message} from "../message";
import {ActivatedRoute} from "@angular/router";
import {ChatService} from "../chat.service";

@Component({
  selector: 'app-users-chat',
  templateUrl: './users-chat.component.html',
  styleUrls: ['./users-chat.component.css']
})
export class UsersChatComponent implements OnInit {

  messages: Message[];
  constructor(private route: ActivatedRoute, private chatService: ChatService) { }

  idReceiver: number;
  idSender: number;

  ngOnInit(): void {
    this.idReceiver=this.route.snapshot.params['user1'];
    this.idSender= parseFloat(window.sessionStorage.getItem('idUser'));
  }

}
