import {Component, Input, OnInit} from '@angular/core';
import {Message} from "../message";
import {SocialUser} from "angularx-social-login";
import {User} from "../../user/user";
import {ActivatedRoute} from "@angular/router";
import {ChatService} from "../chat.service";
import {UserService} from "../../user/user-service.service";

@Component({
  selector: 'app-chat-layout',
  templateUrl: './chat-layout.component.html',
  styleUrls: ['./chat-layout.component.css']
})
export class ChatLayoutComponent implements OnInit {

  userId: number = parseFloat(JSON.parse(localStorage.getItem('userId')));
  idReceiver: number;
  messageMap= new Map<number, Message>();
  conversUsers= new Map<string,User>();
  cUser: any;
  socialsuser= JSON.parse(localStorage.getItem('socialusers'));

  constructor(private route: ActivatedRoute, private chatService: ChatService, private userService: UserService) { }


  ngOnInit(): void {
    this.idReceiver = this.route.snapshot.params['userId'];
    if(this.userId==this.idReceiver){
      this.chatService.getAllConversation(this.userId).subscribe(result=>{
        if(this.idReceiver==result.conversations[0].idSender){
          this.idReceiver=result.conversations[0].idReceiver;
        }else{
          this.idReceiver=result.conversations[0].idSender;
        }
        window.location.replace('chat/viewChats/'+this.idReceiver);
      })
    }
    this.viewChatOfUser(this.userId);
  }

  conv: Message[];
  viewChatOfUser(idUser: number){
    this.chatService.getAllConversation(idUser).subscribe(result =>{
      this.conv=result.conversations;
      var i=0;
      var senderOrReceiver;
      for(i=0;i<this.conv.length;i++){
        if(this.conv[i].idReceiver==this.userId){
          var sender=this.conv[i].idSender;
          if(!this.messageMap.has(this.conv[i].idSender)){
            this.messageMap.set(sender, this.conv[i]);
         /* this.userService.findUserById(sender).subscribe(result=>{
            this.cUser=result[0];
            if(this.cUser.idUser==sender){
              if(!this.conversUsers.has(sender.toString())){
                this.conversUsers.set(sender.toString(),this.cUser);
              }
            }
            console.log(this.conv[i].msg_text)
          });*/}
        }else if(this.conv[i].idSender==this.userId){
          var receiver=this.conv[i].idReceiver;
          if(!this.messageMap.has(receiver)){
            this.messageMap.set(receiver,this.conv[i]);
          }
          /*this.userService.findUserById(receiver).subscribe(result2=>{
            this.cUser=result2[0];
            if(this.cUser.idUser==receiver){
              if(!this.conversUsers.has(receiver.toString())){
                this.conversUsers.set(receiver.toString(),this.cUser);
              }
            }
          });*/
        }
      }
      this.messageMap.forEach((value,key) => {
        this.userService.findUserById(key).subscribe(result=>{
          this.conversUsers.set(key.toString(),result[0])
        }

        )
      });

    })
  }

  goToChat(u1:number,u2:number){
    if (u1==this.userId){
      window.location.replace('chat/viewChats/'+u2);
    }else{
      window.location.replace('chat/viewChats/'+u1);
    }

  }
}
