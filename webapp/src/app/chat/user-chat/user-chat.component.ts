import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-user-chat',
  templateUrl: './user-chat.component.html',
  styleUrls: ['./user-chat.component.css']
})
export class UserChatComponent implements OnInit {

  constructor() { }

  @Input() userId: number = parseFloat(JSON.parse(localStorage.getItem('userId')));

  ngOnInit(): void {
  }

}
