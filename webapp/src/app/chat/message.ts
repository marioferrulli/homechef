export class Message {
  idMessage: number;
  idSender: number;
  idReceiver: number;
  msg_timestamp: Date;
  msg_text: string;
}
