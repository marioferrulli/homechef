import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatsOfAUserComponent } from './chats-of-auser.component';

describe('ChatsOfAUserComponent', () => {
  let component: ChatsOfAUserComponent;
  let fixture: ComponentFixture<ChatsOfAUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChatsOfAUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatsOfAUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
