import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ChatService} from "../chat.service";
import {Message} from "../message";
import {User} from "../../user/user";
import {UserService} from "../../user/user-service.service";

@Component({
  selector: 'app-chats-of-auser',
  templateUrl: './chats-of-auser.component.html',
  styleUrls: ['./chats-of-auser.component.css']
})
export class ChatsOfAUserComponent implements OnInit {

  constructor(private activateRoute: ActivatedRoute, private route: Router, private chatService: ChatService, private userService: UserService) { }

  conversations: Message[];
  @Input() userId: number;
  @Input() messageMap= new Map();
  @Input() conversUsers= new Map();
  @Input() receiver: number;
  cUser: any;
  socialsuser= JSON.parse(localStorage.getItem('socialusers'));


  ngOnInit(): void {
    if(this.userId==null){
    // this.userId=this.activateRoute.snapshot.params['userId'];

  }console.log(this.receiver)
   // this.viewChatOfUser(this.userId);
   // this.showConversation(this.userId);
  var user:User;
    user.facebookId
  }

  newConversation: Message[];

  showConversation(idUser: number){
    this.chatService.getAllConversation(idUser).subscribe(result=>{
      this.conversations=result.conversations;
      var i;var j;
      for(i=0;i<result.conversations;i++){
        for(j=0;j<this.newConversation;j++){
          if(this.newConversation.length!=0)
            if(result.conversations[i].idReceiver==this.newConversation[j].idReceiver){
              if(result.conversations[i].idReceiver==this.newConversation[j].idSender){
              }
            }
        }

      }
    })
  }

  conv: Message[];
  viewChatOfUser(idUser: number){
    this.chatService.getAllConversation(idUser).subscribe(result =>{
      this.conv=result.conversations;
      var i=0;
      var senderOrReceiver;
      for(i=0;i<this.conv.length;i++){
        if(this.conv[i].idReceiver==this.userId){
          var sender=this.conv[i].idSender;
          if(!this.messageMap.has(this.conv[i].idSender)){
            this.messageMap.set(sender, this.conv[i]);
          }
          this.userService.findUserById(sender).subscribe(result=>{
            this.cUser=result[0];
            if(this.cUser.idUser==sender){
              if(!this.conversUsers.has(sender.toString())){
                this.conversUsers.set(sender.toString(),this.cUser);
              }
            }
              console.log(this.conv[i].msg_text)
          });
        }else if(this.conv[i].idSender==this.userId){
          var receiver=this.conv[i].idReceiver;
          if(!this.messageMap.has(receiver)){
            this.messageMap.set(receiver,this.conv[i]);
          }
          this.userService.findUserById(receiver).subscribe(result2=>{
            this.cUser=result2[0];
            if(this.cUser.idUser==receiver){
              if(!this.conversUsers.has(receiver.toString())){
                this.conversUsers.set(receiver.toString(),this.cUser);
              }
            }
          });
        }
      }
    })
  }

  goToChat(u1:number,u2:number){
    if (u1==this.userId){
      window.location.replace('chat/viewChats/'+u2);
    }else{
      window.location.replace('chat/viewChats/'+u1);
    }

  }

}
