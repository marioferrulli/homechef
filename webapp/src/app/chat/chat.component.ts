import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder} from "@angular/forms";
import {ActivatedRoute, Route, Router} from "@angular/router";
import {ChatService} from "./chat.service";
import {Message} from "./message";
import {UserService} from "../user/user-service.service";
import {User} from "../user/user";

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  newMessage= this.formBuilder.group({
    idSender: [''],
    idReceiver: [''],
    msg_text: ['']
  });


  messages: Message[];
  constructor(private router: Router,private formBuilder: FormBuilder, private userService: UserService, private route: ActivatedRoute, private chatService: ChatService) { }


  @Input() idSender: number = parseFloat(JSON.parse(localStorage.getItem('userId')));
  @Input() idReceiver: number ;
  userSender: User;

  ngOnInit(): void {
    this.idReceiver = this.route.snapshot.params['userId'];
    this.getUserReceiver(this.idReceiver);
    this.getMessages();
    this.userSender=JSON.parse(localStorage.getItem('socialusers'));
  }

  userReceiver: User;

  getUserReceiver(userId: number){
    this.userService.findUserById(userId).subscribe(result=>{
      this.userReceiver=result[0];
    })
  }


  getMessages(){
    this.chatService.getConversation(this.idSender,this.idReceiver).subscribe(data=>{
        this.messages=data.messageList;
      }
    )
  }


  sendAMessage(){
    window.location.replace('chat/viewChats/'+this.idReceiver);
    this.newMessage.patchValue({
      idReceiver: this.idReceiver
    });
    this.newMessage.patchValue({
      idSender: this.idSender
    });
    console.log(JSON.stringify(this.newMessage.getRawValue()));
    this.chatService.addMessageAndGetConversation(this.newMessage).subscribe(result=>{
      this.messages=result.messageList;
      console.log(result);
    })

  }

}
