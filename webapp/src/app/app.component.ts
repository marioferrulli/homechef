import {Component, OnInit} from "@angular/core";
import {FacebookLoginComponent} from "./user/facebook-login/facebook-login.component";
import {UserService} from "./user/user-service.service";
import {AuthService, SocialUser} from "angularx-social-login";
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title: string;
  name: string;
  log: boolean;
  img: string;
  socialusers: SocialUser=JSON.parse(localStorage.getItem('socialusers'));

  constructor(private authService: AuthService, private route:Router) {
    this.title = 'Home Chef';
  }

  ngOnInit(): void {
    if(this.socialusers!=null){
      this.name=this.socialusers.name;
      this.img=this.socialusers.photoUrl;
    }
  }



}
