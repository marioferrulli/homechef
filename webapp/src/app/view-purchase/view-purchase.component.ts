import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Route, Router} from "@angular/router";
import {PurchaseService} from "../purchase/purchase.service";
import {InsertionServiceService} from "../insertions/insertions.service";
import {Insertion} from "../insertions/insertion";
import {ReviewService} from "../review/review.service";

@Component({
  selector: 'app-view-purchase',
  templateUrl: './view-purchase.component.html',
  styleUrls: ['./view-purchase.component.css']
})
export class ViewPurchaseComponent implements OnInit {

  constructor(private reviewService: ReviewService,private router: Router,private route: ActivatedRoute, private purchaseService: PurchaseService, private insertionsService: InsertionServiceService) { }

  ngOnInit(): void {
    this.showElements();

  }
  id: number;
  idBuyer: any;
  insertion: Insertion;
  insertions: Insertion[] = [];
  results: any[];
  imagesToShow= new Map();
  showElements(){
    this.id=this.route.snapshot.params['userId'];
    this.insertionsService.getInactiveInsertions().subscribe(result=>{
      this.results=result;
      var i=0;
      if(this.results.length!=0) {
        for (i = 0; i < result.length; i++) {
          this.getUserPurchase(this.results[i]);
        }
      }
    });
  }


  getUserPurchase(result: Insertion){
      this.purchaseService.findBuyerByIdInsertion(result.idInsertion).subscribe(id =>{
        this.idBuyer=id;
        console.log(id)
        if(id==this.id){
          this.insertion=result;
          this.insertions.push(this.insertion);
          this.imagesToShow=this.insertionsService.getImages(this.insertions);
        }
      })
    }

   addReview(id: number){
      this.router.navigate(['/purchases/addReview/'+id]);
   }

}
