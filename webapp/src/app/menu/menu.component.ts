import {Component, EventEmitter, Input, NgModule, OnInit, Output} from '@angular/core';
import { faSearch, faBell, faUser } from '@fortawesome/free-solid-svg-icons';
import {AuthService, SocialUser} from "angularx-social-login";
import {ActivatedRoute, Router} from "@angular/router";
import {FacebookLoginComponent} from "../user/facebook-login/facebook-login.component";
import {UserService} from "../user/user-service.service";
import {InsertionServiceService} from "../insertions/insertions.service";
import {ChatService} from "../chat/chat.service";
import {MatMenuTrigger} from "@angular/material/menu";
import {Insertion} from "../insertions/insertion";
import {InsertionsPageComponent} from "../insertions/insertions-page/insertions-page.component";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css'],
  providers: [UserService, InsertionServiceService],
})

export class MenuComponent implements OnInit {

  imagestoShow= new Map();
  insertions : Insertion[];
  title: string;
  name: string;
  log: boolean;
  img: string;
  constructor(private chatService: ChatService,private authService: AuthService, private actRoute: ActivatedRoute, private route:Router, private insertionsService: InsertionServiceService, private userService: UserService) {
    this.title = 'Home Chef';
  }

  socialuser = JSON.parse(localStorage.getItem('socialusers'));
  userId: number = parseFloat(JSON.parse(localStorage.getItem('userId')));

  ngOnInit(): void {
    console.log("noOK")
    if(this.socialuser!=null){
      console.log("OK!")
      this.log=true;
      this.img=this.socialuser.picture.data.url;
      this.name=this.socialuser.name;
    }
  }

  goToProfile(){
    window.location.replace("/users/profile/"+this.userId);
  }

  logout(){
    this.userService.fbLogout();
  }

  goToMyInsertions(){
    window.location.replace('/insertions/profile/'+this.userId)
  }

  goToMyPurchase(){
    window.location.replace('purchases/'+this.userId)
  }

  goToMyReview(){
    window.location.replace('reviews/viewReview/'+this.userId)
  }

  goToMyChats(){
    window.location.replace('chat/viewChats/'+this.userId)
  }



  goToActiveInsertions(){
    window.location.replace('insertions/viewInsertions/insertions');
  }

  goToActiveRequest(){
    window.location.replace('requests/viewRequests/menu/requests')
  }

  goToAddRequest(){
    window.location.replace('requests/insertNewRequest')
  }

  goToMyRequests(){
    window.location.replace('requests/viewRequests/'+this.userId)
  }

  goToAddInsertion(){
    window.location.replace('addInsertion');
  }


  price: boolean = false;
  param: any;
  giveInsertionOrderPriceA(){
    window.location.replace('insertions/viewInsertions/priceA');
  }
  giveInsertionOrderPriceD(){
    window.location.replace('insertions/viewInsertions/priceD')
  }
  giveInsertionOrderDateA(){
    window.location.replace('insertions/viewInsertions/dateA')
  }
  giveInsertionOrderDateD(){
    window.location.replace('insertions/viewInsertions/dateD');

  }

  giveRequestOrderPriceA(){
    window.location.replace('requests/viewRequests/menu/priceA')
  }
  giveRequestOrderPriceD(){
    window.location.replace('requests/viewRequests/menu/priceD')
  }
  giveRequestOrderDateA(){
    window.location.replace('requests/viewRequests/menu/dateA')
  }
  giveRequestOrderDateD(){
    window.location.replace('requests/viewRequests/menu/dateD')
  }


}
