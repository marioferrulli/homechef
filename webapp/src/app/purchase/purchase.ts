import {User} from "../user/user";
import {Insertion} from "../insertions/insertion";

export class Purchase {
  user: User;
  insertion = Insertion;
  date = Date;
}
