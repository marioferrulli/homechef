import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {FormGroup} from "@angular/forms";
import {Observable} from "rxjs";
import {Insertion} from "../insertions/insertion";
import {retry} from "rxjs/operators";
import {Purchase} from "./purchase";

@Injectable({
  providedIn: 'root'
})
export class PurchaseService {
  addPurchaseUrl: string
  public urltofindBuyerByIdInsertion: string;

  constructor(private httpClient: HttpClient) {
    this.addPurchaseUrl= "http://localhost:8080/purchases/addPurchase";
    this.urltofindBuyerByIdInsertion="http://localhost:8080/purchases/findBuyerByIdInsertion/";
  }

  public findBuyerByIdInsertion(idInsertion: number){
    return this.httpClient.get(this.urltofindBuyerByIdInsertion+idInsertion);
  }

  public savePurchase(purchase: FormGroup): Observable<Insertion> {
    return this.httpClient.post<Insertion>("http://localhost:8080/purchases/addPurchase", JSON.stringify(purchase.getRawValue())).pipe(
      retry(1))
  }



}
