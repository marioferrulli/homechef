import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewAReviewComponent } from './view-areview.component';

describe('ViewAReviewComponent', () => {
  let component: ViewAReviewComponent;
  let fixture: ComponentFixture<ViewAReviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewAReviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewAReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
