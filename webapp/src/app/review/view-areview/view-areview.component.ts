import { Component, OnInit } from '@angular/core';
import {ReviewService} from "../review.service";
import {ActivatedRoute} from "@angular/router";
import {Review} from "../review";
import {ReviewResponse} from "../review-response";

@Component({
  selector: 'app-view-areview',
  templateUrl: './view-areview.component.html',
  styleUrls: ['./view-areview.component.css']
})
export class ViewAReviewComponent  implements OnInit {

  constructor(private reviewService: ReviewService, private route: ActivatedRoute) { }

  rating= new Array(5);
  reviewId:number;
  review: Review
  responseReview: ReviewResponse;
  userid= parseFloat(JSON.parse(localStorage.getItem('userId')));

  ngOnInit(): void {
    this.reviewId=this.route.snapshot.params['reviewId'];
    this.reviewService.findById(this.reviewId).subscribe(result=>{
      this.review=result[0];
      var responseUser=this.review.insertion.user.idUser
    this.reviewService.findResponse(this.reviewId,responseUser).subscribe(result=>{
      this.responseReview=result[0];
      if(result.length!=0){
        this.response=true;
      }
    });
    })
  }

  response: boolean=false;

}
