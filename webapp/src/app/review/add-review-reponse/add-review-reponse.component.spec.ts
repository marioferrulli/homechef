import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddReviewReponseComponent } from './add-review-reponse.component';

describe('AddReviewReponseComponent', () => {
  let component: AddReviewReponseComponent;
  let fixture: ComponentFixture<AddReviewReponseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddReviewReponseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddReviewReponseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
