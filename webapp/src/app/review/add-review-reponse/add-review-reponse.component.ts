import { Component, OnInit } from '@angular/core';
import {FormBuilder} from "@angular/forms";
import {User} from "../../user/user";
import {Review} from "../review";
import {ActivatedRoute, Router} from "@angular/router";
import {ReviewService} from "../review.service";
import {SocialUser} from "angularx-social-login";
import {UserService} from "../../user/user-service.service";

@Component({
  selector: 'app-add-review-reponse',
  templateUrl: './add-review-reponse.component.html',
  styleUrls: ['./add-review-reponse.component.css']
})
export class AddReviewReponseComponent implements OnInit {

  review: Review;
  rating= new Array(5);
  newReviewResponse= this.formBuilder.group({
    user: [''],
    review: [''],
    text: [''],
  });
  reviewId: number;
  socialuser: SocialUser=JSON.parse(localStorage.getItem('socialusers'));
  user: User;
  currentUser: number = parseFloat(JSON.parse(localStorage.getItem('userId')));

  constructor(private userService: UserService,private router: Router,private formBuilder: FormBuilder, private route: ActivatedRoute, private reviewService: ReviewService) { }

  ngOnInit(): void {

    if(this.socialuser!=null){
      this.reviewId=this.route.snapshot.params['reviewId'];
      this.getDate(this.reviewId);
      this.reviewService.findById(this.reviewId).subscribe(result=>{
        this.review=result[0];
        this.findResponse();
      })
    }


  }
  response: boolean=false;
  goReview(){
    this.response=true;
    this.ngOnInit();
  }

  findResponse(){
    this.reviewService.findResponse(this.review.idReview, this.review.insertion.user.idUser).subscribe(result=>{
      var idResponse=result;
      if(result.length>0){
        this.router.navigate(['reviews/viewAReview/'+this.reviewId]);
      }

    }

    )
  }

  getDate(reviewId: number){
    this.newReviewResponse.patchValue({
      user: { idUser: parseFloat(JSON.parse(localStorage.getItem('userId')))}
    });
    this.newReviewResponse.patchValue({
      review:{ idReview: reviewId}
    })
  }


  responseSaved=false;

  saveReviewResponse(){
    this.reviewService.addReviewResponse(this.newReviewResponse.getRawValue()).subscribe(result=>{
      var idResponse=result;
      this.responseSaved=true;
      this.router.navigate(['reviews/viewAReview/'+this.reviewId])
    }
    )
  }
}
