import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ReviewService} from "../review.service";
import {Review} from "../review";
import {FormBuilder} from "@angular/forms";
import {UserService} from "../../user/user-service.service";
import {User} from "../../user/user";
import {SocialUser} from "angularx-social-login";

@Component({
  selector: 'app-view-review',
  templateUrl: './view-review.component.html',
  styleUrls: ['./view-review.component.css']
})
export class ViewReviewComponent implements OnInit {

  constructor(private userService: UserService,private route: ActivatedRoute, private reviewService: ReviewService, private router: Router) { }
  userReviewId= this.route.snapshot.params['userId'];
  reviews: Review[];
  ngOnInit(): void {

    this.showReview(this.route.snapshot.params['userId']);
    this.searchUser(this.userReviewId);
  }

  rating=new Array(5)
  datas: User[];
  user: User;
  socialuser: SocialUser=JSON.parse(localStorage.getItem('socialusers'));

  searchUser(userId:number){
    //this.userId=this.route.snapshot.params['userId'];
    this.userService.findUserById(userId).subscribe(result=>{
      this.datas=result;
      this.user=this.datas[0];
    })
  }


  showReview(idUser:number){
    this.reviewService.findReceivedReviews(idUser).subscribe(result=>{
      this.reviews=result;
    })
  }

  responseBool: boolean;
  getResponse(review: Review): boolean{
    this.reviewService.findResponse(review.idReview, this.userReviewId).subscribe(result=>{
      var response=result[0];
      if(response){
        this.responseBool=true
        return this.responseBool;
      }
    })
    return this.responseBool;
  }


  response(idReview: number){
    this.router.navigate(['/responses/addReviewResponse/'+idReview]);
  }


}
