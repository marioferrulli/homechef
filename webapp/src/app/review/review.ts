import {Insertion} from "../insertions/insertion";
import {User} from "../user/user";

export class Review {
  idReview: number;
  insertion: Insertion;
  user: User;
  rating: number;
  text: string;
  date: Date;
}
