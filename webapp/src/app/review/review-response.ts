import {User} from "../user/user";
import {Review} from "./review";

export class ReviewResponse {
  user: User;
  review: Review;
  text: string;
}
