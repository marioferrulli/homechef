import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Review} from "./review";
import {retry} from "rxjs/operators";
import {FormGroup} from "@angular/forms";
import {Observable} from "rxjs";
import {ReviewResponse} from "./review-response";

@Injectable({
  providedIn: 'root'
})
export class ReviewService {
public urlAddReview: string;
public urlFindById: string;
public urlfindRecievedReview: string;
public urlEdit: string;
public urlAddReviewResponse: string;
public urlAverage: string;
public urlfindresponse:string;
public  urlfindreview: string;

  constructor(private http: HttpClient) {
    this.urlAddReview="http://localhost:8080/reviews/addReview";
    this.urlFindById="http://localhost:8080/reviews/findById/";
    this.urlfindRecievedReview="http://localhost:8080/reviews/findReceivedReviews/";
    this.urlEdit="http://localhost:8080/reviews/edit/"
    this.urlAddReviewResponse="http://localhost:8080/responses/addReviewResponse/";
    this.urlAverage= "http://localhost:8080/reviews/getAverageRatingForUser/";
    this.urlfindresponse="http://localhost:8080/responses/findResponsesByReviewAndUser/";
    this.urlfindreview="http://localhost:8080/reviews/findReviewByUserAndInsertion/?";
  }

  findReviewByUserAndInsertion(userId:number, insertionId:number): Observable<any>{
    return this.http.get<any>(this.urlfindreview+'user='+userId+'&insertion='+insertionId);
  }

  addReview(review: FormGroup): Observable<Review>{
    return this.http.post<Review>(this.urlAddReview, JSON.stringify(review.getRawValue())).pipe(
      retry(1)
    )
  }

  findResponse(idReview: number, idUser: number): Observable<ReviewResponse[]>{
    return this.http.get<ReviewResponse[]>(this.urlfindresponse+'?idReview='+idReview+'&idUser='+idUser)
  }

  findById(idReview: number): Observable<Review>{
    return this.http.get<Review>(this.urlFindById+idReview);
  }

  //cerca le review ricevute
  findReceivedReviews(idUser: number):Observable<Review[]>{
    return this.http.get<Review[]>(this.urlfindRecievedReview+idUser);
  }

  editReview(newReview: Review, idUser:number){
    return this.http.put(this.urlEdit+idUser,{newReview})
  }

  //ReviewResponses
  addReviewResponse(reviewResponse: ReviewResponse): Observable<ReviewResponse>{
    return this.http.post<ReviewResponse>(this.urlAddReviewResponse, JSON.stringify(reviewResponse)).pipe(
      retry(1)
    );
  }

  getAverageRating(userId: number): Observable<any>{
    return this.http.get<any>(this.urlAverage+userId);
  }



}
