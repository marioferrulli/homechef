import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {User} from "../../user/user";
import {ActivatedRoute, Route, Router} from "@angular/router";
import {Insertion} from "../../insertions/insertion";
import {PurchaseService} from "../../purchase/purchase.service";
import {ReviewService} from "../review.service";
import {SocialUser} from "angularx-social-login";

@Component({
  selector: 'app-add-review',
  templateUrl: './add-review.component.html',
  styleUrls: ['./add-review.component.css']
})
export class AddReviewComponent implements OnInit {
insertion: Insertion;
socialuser: SocialUser=JSON.parse(localStorage.getItem('socialusers'));
  idUser = parseFloat(JSON.parse(localStorage.getItem('userId')));
  newReview=this.formBuilder.group({
    insertion: [''],
    user: [''],
    rating: [''],
    text: ['']
  })
  constructor(private router: Router,private formBuilder: FormBuilder,private route: ActivatedRoute, private reviewService: ReviewService,private purchaseService: PurchaseService) { }

  ngOnInit(): void {
    if (this.socialuser!=null){
      this.addDate();
    }
  }

  saveReview() {
    //console.log(JSON.stringify(this.newReview))
    console.log('Review'+JSON.stringify(this.newReview.getRawValue()))
    var review= this.newReview.getRawValue();
    this.reviewService.addReview(this.newReview).subscribe(result=>
    {
      var idReview=result;
      this.router.navigate(['reviews/viewAReview/'+idReview])
    });

  }


  addDate(){
    var idInsertion =  this.route.snapshot.params['insertionId'];
    var idUser: any;
    this.newReview.patchValue({
      insertion: {idInsertion: idInsertion}
    });
    this.purchaseService.findBuyerByIdInsertion(idInsertion).subscribe(id =>{
      idUser=id;
      this.newReview.patchValue({
        user: {idUser: idUser}
      });
      });

  }
}
