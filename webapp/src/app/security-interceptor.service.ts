import {Injectable, Injector} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpHeaders, HttpResponse} from "@angular/common/http";
import {Observable, of} from "rxjs";
var header;
var jwt;

@Injectable({
  providedIn: 'root'
})
export class SecurityInterceptorService implements HttpInterceptor{

  constructor(private injector: Injector) {
  }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if(window.sessionStorage.getItem("image") && window.sessionStorage.getItem('jwtToken')){
      jwt= window.sessionStorage.getItem('jwtToken');
      window.sessionStorage.removeItem('image')
      // Clone the request and set the new header in one step.
      req = req.clone({ setHeaders: {'Authorization': jwt } });
      //correctReq = req.clone({
      /**     headers: req.headers.set('Authorization',
       window.sessionStorage.getItem('jwtToken'))*/
      //headers:  new HttpHeaders({'Content-Type':'application/json; charset=utf-8','Authorization': jwt})
      //});
      header=req.headers.get('Authorization');
      return next.handle(req);
    }
    else if (window.sessionStorage.getItem('jwtToken') && !window.sessionStorage.getItem("image")) {

      jwt= window.sessionStorage.getItem('jwtToken');
      // Clone the request and set the new header in one step.
      req = req.clone({ setHeaders: { 'Content-Type': 'application/json; charset=utf-8','Authorization': jwt } });
      //correctReq = req.clone({
   /**     headers: req.headers.set('Authorization',
          window.sessionStorage.getItem('jwtToken'))*/
      //headers:  new HttpHeaders({'Content-Type':'application/json; charset=utf-8','Authorization': jwt})
      //});
      header=req.headers.get('Authorization');
      return next.handle(req);
    }else{
      return next.handle(req);
    }
  }
}
