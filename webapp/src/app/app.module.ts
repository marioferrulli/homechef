import {NgModule} from "@angular/core";
import {AppComponent} from "./app.component";
import {UserListComponent} from "./user/user-list/user-list.component";

import {FacebookLoginComponent} from "./user/facebook-login/facebook-login.component";
import {BrowserModule} from "@angular/platform-browser";
import {AppRoutingModule} from "./app-routing.module";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {UserService} from "./user/user-service.service";

import { DashboardComponent } from './dashboard/dashboard.component';
import { HomeComponent } from './home/home.component';
import { FormInsertionsComponent } from './insertions/form-insertions.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {InsertionServiceService} from "./insertions/insertions.service";
import {SecurityInterceptorService} from "./security-interceptor.service";
import {AuthService, AuthServiceConfig, FacebookLoginProvider, SocialLoginModule} from "angularx-social-login";
import { InsertionDetailsComponent } from './insertions/insertion-details/insertion-details.component';
import { FacebookLogoutComponent } from './user/facebook-logout/facebook-logout.component';
import { CreateInsertionComponent } from './insertions/create-insertion/create-insertion.component';
import { RequestComponent } from './request/request.component';
import { CheckPageRequestComponent } from './request/check-page-request/check-page-request.component';
import { CreateRequestComponent } from './request/create-request/create-request.component';
import { UploadImageComponent } from './insertions/upload-image/upload-image.component';
import { ImageComponent } from './insertions/image/image.component';
import { MenuComponent } from './menu/menu.component';
import {MatButtonModule} from "@angular/material/button";
import {MatMenuModule} from "@angular/material/menu";
import {MatCommonModule, MatNativeDateModule} from "@angular/material/core";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import { ProfiloComponent } from './user/profilo/profilo.component';
import {MatCardModule} from "@angular/material/card";
import { InsertionsByIdComponent } from './insertions/insertions-by-id/insertions-by-id.component';
import { RequestByIdComponent } from './request/request-by-id/request-by-id.component';
import { ViewPurchaseComponent } from './view-purchase/view-purchase.component';
import { AddReviewComponent } from './review/add-review/add-review.component';
import { ViewReviewComponent } from './review/view-review/view-review.component';
import { AddReviewReponseComponent } from './review/add-review-reponse/add-review-reponse.component';
import { ChatComponent } from './chat/chat.component';
import { ChatsOfAUserComponent } from './chat/chats-of-auser/chats-of-auser.component';
import { UsersChatComponent } from './chat/users-chat/users-chat.component';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import {MatSliderModule} from "@angular/material/slider";
import {MatRadioModule} from "@angular/material/radio";
import { UserCurrentprofileComponent } from './user/user-currentprofile/user-currentprofile.component';
import {MatIconModule} from "@angular/material/icon";
import { ShopWindowComponent } from './insertions/shopWindow/shop-window.component';
import { InsertionsPageComponent } from './insertions/insertions-page/insertions-page.component';
import { RequestsPageComponent } from './request/requests-page/requests-page.component';
import { ShopWindowWithoutImageComponent } from './request/shop-window-without-image/shop-window-without-image.component';
import { RequestDetailComponent } from './request/request-detail/request-detail.component';
import { ChatViewComponent } from './chat/chat-view/chat-view.component';
import {MatButtonToggleModule} from "@angular/material/button-toggle";
import {NgScrollbarModule} from "ngx-scrollbar";
import {PlatformModule} from '@angular/cdk/platform';
import { ChatLayoutComponent } from './chat/chat-layout/chat-layout.component';
import { UserChatComponent } from './chat/user-chat/user-chat.component';
import { ViewAReviewComponent } from './review/view-areview/view-areview.component';
import { ViewUserInsertionsComponent } from './insertions/view-user-insertions/view-user-insertions.component';


export function tokenGetter() {
  return window.sessionStorage.getItem('jwtToken');
}
let config = new AuthServiceConfig([
  /**  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider("Google-OAuth-Client-Id")
  },**/
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider("453715758590837")
  }
]);
export function provideConfig() {
  return config;
}





@NgModule({
  declarations: [
    AppComponent,
    UserListComponent,
    FacebookLoginComponent,
    DashboardComponent,
    HomeComponent,
    FormInsertionsComponent,
    InsertionDetailsComponent,
    FacebookLogoutComponent,
    CreateInsertionComponent,
    RequestComponent,
    CheckPageRequestComponent,
    CreateRequestComponent,
    UploadImageComponent,
    ImageComponent,
    MenuComponent,
    ProfiloComponent,
    InsertionsByIdComponent,
    RequestByIdComponent,
    ViewPurchaseComponent,
    AddReviewComponent,
    ViewReviewComponent,
    AddReviewReponseComponent,
    ChatComponent,
    ChatsOfAUserComponent,
    UsersChatComponent,
    UserCurrentprofileComponent,
    ShopWindowComponent,
    InsertionsPageComponent,
    RequestsPageComponent,
    ShopWindowWithoutImageComponent,
    RequestDetailComponent,
    ChatViewComponent,
    ChatLayoutComponent,
    UserChatComponent,
    ViewAReviewComponent,
    ViewUserInsertionsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    SocialLoginModule,
    FormsModule,
    MatMenuModule,
    MatButtonModule,
    MatCommonModule,
    BrowserAnimationsModule,
    FontAwesomeModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSlideToggleModule,
    MatSliderModule,
    MatRadioModule,
    MatIconModule,
    MatButtonToggleModule,
  ],
  providers: [UserService, InsertionServiceService, AuthService,
    {provide: HTTP_INTERCEPTORS, useClass: SecurityInterceptorService, multi: true},
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }
