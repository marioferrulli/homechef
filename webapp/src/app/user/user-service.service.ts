import { User } from './user';
import {Observable} from "rxjs/";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {AuthService, FacebookLoginProvider, SocialUser} from "angularx-social-login";
import {retry} from "rxjs/operators";
import {Injectable} from "@angular/core";


declare const FB: any;
var userId;
var token;
@Injectable()

export class UserService {

  public addUserUrl: string;
  isLogged: boolean;
  user: any=SocialUser;
  myuser: any;
  public toGetUserUrl: string;
  idUser: number;
  public idUserUrl: string;

  constructor(private http: HttpClient, private route: Router,private authService: AuthService) {
    (window as any).fbAsyncInit = function() {
      FB.init({
        appId :  '453715758590837',
        status : false,
        cookie : false,
        xfbml  : true,
        version : 'v5.0'
      });
      FB.AppEvents.logPageView();
    };

    (function(d, s, id){
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {return;}
      js = d.createElement(s); js.id = id;
      js.src = "http://connect.facebook.net/it_IT/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));


    this.toGetUserUrl= "http://localhost:8080/users/findById/";
    this.addUserUrl = "http://localhost:8080/users/addUsers";
    this.idUserUrl="http://localhost:8080/users/findById/"
  }

  public findUserById(idUser:number): Observable<any>{
    return this.http.get(this.idUserUrl+idUser);
  }

  public findAll(): Observable<User[]> {
    return this.http.get<User[]>(this.addUserUrl);
  }

  public save(user: User) {
    return this.http.post<User>(this.addUserUrl, user).pipe(
      retry(1)
    );
  }


   fbLoginStatus(): any{
     FB.getLoginStatus(function (response) {
          return response;
     })

   }

    fbLogin() {
   //   return new Promise((resolve, reject) => {
        FB.login(result => {
          if(result.  authResponse){
            FB.api('/me', 'GET',{fields: 'last_name, first_name, name,id,picture.width(150).height(150)'}, function(response) {
              console.log(response);
              localStorage.clear();
              localStorage.setItem('socialusers', JSON.stringify(response));
            });
          userId = result.authResponse
            ? result.authResponse.userID
            : null;
         /* return this.http.post<{jwtToken:  string}>('http://localhost:8080/login/facebook', {"userId": result.authResponse.userID,"token": result.authResponse.accessToken}).pipe(tap(res => {
            localStorage.setItem('jwtToken', res.jwtToken);}))*/
          //if (result.authResponse) {

         this.http.post('http://localhost:8080/login/facebook/webApp', {"userId": userId,"token": result.authResponse.accessToken})
              .toPromise()
              .then(response => {
                token = response;
                if (token) {
                  localStorage.setItem('userId', JSON.stringify(token.idUserSpring));
                  let tokenSTR= 'Bearer '+ token.jwtToken;
                  window.sessionStorage.setItem('jwtToken', tokenSTR);
                  window.location.replace('/home');
                }
              /*  this.authService.signIn(FacebookLoginProvider.PROVIDER_ID).then((userData =>
                {
                  this.user = userData;
                  localStorage.setItem('socialusers', JSON.stringify( this.user));
                  localStorage.setItem('userId', JSON.stringify(token.idUserSpring));
                  this.route.navigate(['home'])
                }));*/
              })
          };
        },{ scope: 'public_profile,email' });
  }


  result: any;
  fbLogout() {
    FB.getLoginStatus(function(response) {
      if (response && response.status === 'connected') {
        FB.logout(function(response) {
          this.isLogged=false;
          window.location.replace('/login');
        });
      }
    });
   // this.route.navigate(['/login']);
  }


}
