import { Component, OnInit } from '@angular/core';
import {UserService} from "../user-service.service";
import {AuthService, FacebookLoginProvider, SocialUser} from "angularx-social-login";
import {ActivatedRoute, Router} from "@angular/router";
import {User} from "../user";
import {ReviewService} from "../../review/review.service";
import {Review} from "../../review/review";
import {InsertionServiceService} from "../../insertions/insertions.service";
import {Insertion} from "../../insertions/insertion";
import {Observable} from "rxjs";
import {RequestService} from "../../request/request.service";
import {Request} from "../../request/request";
declare var FB: any;

@Component({
  selector: 'app-profilo',
  templateUrl: './profilo.component.html',
  styleUrls: ['./profilo.component.css']
})
export class ProfiloComponent implements OnInit {

  constructor( private requestService: RequestService,private route: ActivatedRoute, private router: Router,private userService: UserService, private reviewService: ReviewService, private insertionService: InsertionServiceService) { }
  img: string;
  userId: number;
  user: User;
  datas: User[];
  reviews: Review[];
  insertionName: string;
  currentUser: number = parseFloat(JSON.parse(localStorage.getItem('userId')));
  socialuser = JSON.parse(localStorage.getItem('socialusers'));

  ngOnInit(): void {
    this.userId=this.route.snapshot.params['userId'];


   this.searchUser(this.userId);
   this.getReview();
   this.getSeller();
   this.getRequests();
   this.getAverageRating(this.userId);
  }

  searchUser(userId:number){
    //this.userId=this.route.snapshot.params['userId'];
    this.userService.findUserById(userId).subscribe(result=>{
      this.datas=result;
      this.user=this.datas[0];
      if(this.userId==this.currentUser){
        this.img=this.socialuser.picture.data.url;
      }else{
        this.img="http://graph.facebook.com/" + this.user.facebookId + "/picture?type=large";
      }
    })
  }

  reviewLenght: number;
  rating= new Array(5);
  getReview(){
    this.reviewService.findReceivedReviews(this.userId).subscribe( result=>{
        this.reviews=result;
        this.reviewLenght=this.reviews.length;

      }
    )
  }

  ins: Insertion;
/*
  getInsertion(idIns: number){
    this.insertionService.getInsertionById(idIns).subscribe(result=>{
      this.ins=result[0];
      }
    )
  }*/

  ilenght: number;
  insertions: Insertion[];
  imagestoShow= new Map();
  getSeller(){
    this.insertionService.getInsertionBySeller(this.userId).subscribe(result=>{
      this.imagestoShow=this.insertionService.getImages(result);

      this.insertions=result;
      this.ilenght=this.insertions.length
    })
  }

  rlenght: number;
  threeRequests: Request[]
  requests: Request[];
  getRequests(){
    this.requestService.findBySeller(this.userId).subscribe(result=>{
      this.requests=result;
      this.rlenght=this.requests.length;
    })
  }

  goToRequests(){
    this.router.navigate(['requests/viewRequests/'+this.userId]);
  }

  showElement(idInsertion: number){
    this.insertionService.setIdInsertions(idInsertion);
  }

  goToChat(){
    this.router.navigate(['chat/viewChats/'+this.userId]);
  }

  numbers= new Array<number>();
  whiteStar= new Array();
  average: any;
  getAverageRating(userId:number){
    this.reviewService.getAverageRating(userId).subscribe(result=>{
      this.average=result;
      var i;
      if(this.average>0){
        for(i=0;i<this.average;i++){
          this.numbers.push(i)
        }
        var j;
        for(j=0;j<(5-this.average);j++){
          this.whiteStar.push(j);
        }
      }

    })
  }
}
