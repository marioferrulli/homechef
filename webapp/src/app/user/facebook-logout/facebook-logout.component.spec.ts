import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacebookLogoutComponent } from './facebook-logout.component';

describe('FacebookLogoutComponent', () => {
  let component: FacebookLogoutComponent;
  let fixture: ComponentFixture<FacebookLogoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacebookLogoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacebookLogoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
