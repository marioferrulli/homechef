import { Component, OnInit } from '@angular/core';
import {UserService} from "../user-service.service";
import {Router} from "@angular/router";
import {SocialUser} from "angularx-social-login";

@Component({
  selector: 'app-facebook-logout',
  templateUrl: './facebook-logout.component.html',
  styleUrls: ['./facebook-logout.component.css']
})
export class FacebookLogoutComponent implements OnInit {

  loggedIn=false;
  constructor(
    private userService: UserService,
    private router: Router,
  ) { }

  user: SocialUser= JSON.parse(localStorage.getItem('socialusers'));
  img=this.user.photoUrl;
  name=this.user.name;
  surname=this.user.lastName;

  ngOnInit() {

  }

  goLogin(){
    localStorage.removeItem('socialusers');
    localStorage.removeItem('userId');
    localStorage.clear();
    console.log(localStorage.getItem('socialusers'));
    this.router.navigate(['login']);
  }

  fbLogout() {
    this.userService.fbLogout();
    this.loggedIn=false;
  }


  goHome(){
    this.userService.fbLogin();
    this.router.navigate(['/home']);
  }
}
