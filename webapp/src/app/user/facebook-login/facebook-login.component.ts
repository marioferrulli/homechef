
import {UserService} from "../user-service.service";
import {Router, RouterModule} from "@angular/router";
import {Component, OnInit} from "@angular/core";
import {AppComponent} from "../../app.component";
import {AuthService, FacebookLoginProvider, SocialUser} from "angularx-social-login";
var FB:any;

@Component({
  selector: 'app-facebook-login',
  templateUrl: './facebook-login.component.html',
  styleUrls: ['./facebook-login.component.css'],
  providers: [UserService]
})
export class FacebookLoginComponent implements OnInit {

  img: string;
  user: any = SocialUser;
  isLogged: boolean;
  socialuser;
  constructor(
    private userService: UserService,
    private router: Router,
    private authService: AuthService,
  ) { }
  ngOnInit() {
    this.socialuser=JSON.parse(localStorage.getItem('socialusers'));
    if(this.socialuser!=null){
      this.img=this.socialuser.picture.data.url;
    }
    var response=this.userService.fbLoginStatus();
    if(response=='connected'){
      window.location.replace('/home');
      this.img=this.socialuser.picture.data.url;
    }
  }


  goHome(){
    this.userService.fbLogin();
  }

  goLogin(){
    localStorage.clear();
    console.log(localStorage.getItem('socialusers'));
    this.userService.fbLogout();
    this.ngOnInit();
  }

  fbLogin() {
    this.userService.fbLogin();
    this.ngOnInit();
  }
}
