export class User {
  idUser: number;
  name: string;
  surname: string;
  email: string;
  facebookId: string;
  githubId: string;

}

