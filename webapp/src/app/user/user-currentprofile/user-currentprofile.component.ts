import { Component, OnInit } from '@angular/core';
import {SocialUser} from "angularx-social-login";

@Component({
  selector: 'app-user-currentprofile',
  templateUrl: './user-currentprofile.component.html',
  styleUrls: ['./user-currentprofile.component.css']
})
export class UserCurrentprofileComponent implements OnInit {

  constructor() { }
  img: string;
  socialusers: SocialUser= JSON.parse(localStorage.getItem('socialusers'));

  ngOnInit(): void {
    this.img=this.socialusers.photoUrl;
  }

}
