import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserCurrentprofileComponent } from './user-currentprofile.component';

describe('UserCurrentprofileComponent', () => {
  let component: UserCurrentprofileComponent;
  let fixture: ComponentFixture<UserCurrentprofileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserCurrentprofileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserCurrentprofileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
