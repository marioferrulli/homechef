
import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {FacebookLoginComponent} from "./user/facebook-login/facebook-login.component";

import {DashboardComponent} from "./dashboard/dashboard.component";
import {HomeComponent} from "./home/home.component";
import {FormInsertionsComponent} from "./insertions/form-insertions.component";
import {InsertionDetailsComponent} from "./insertions/insertion-details/insertion-details.component";
import {CreateInsertionComponent} from "./insertions/create-insertion/create-insertion.component";
import {RequestComponent} from "./request/request.component";
import {CheckPageRequestComponent} from "./request/check-page-request/check-page-request.component";
import {CreateRequestComponent} from "./request/create-request/create-request.component";
import {FacebookLogoutComponent} from "./user/facebook-logout/facebook-logout.component";
import {UploadImageComponent} from "./insertions/upload-image/upload-image.component";
import {ProfiloComponent} from "./user/profilo/profilo.component";
import {InsertionsByIdComponent} from "./insertions/insertions-by-id/insertions-by-id.component";
import {ViewPurchaseComponent} from "./view-purchase/view-purchase.component";
import {AddReviewComponent} from "./review/add-review/add-review.component";
import {ViewReviewComponent} from "./review/view-review/view-review.component";
import {AddReviewReponseComponent} from "./review/add-review-reponse/add-review-reponse.component";
import {ChatComponent} from "./chat/chat.component";
import {ChatsOfAUserComponent} from "./chat/chats-of-auser/chats-of-auser.component";
import {UsersChatComponent} from "./chat/users-chat/users-chat.component";
import {RequestByIdComponent} from "./request/request-by-id/request-by-id.component";
import {InsertionsPageComponent} from "./insertions/insertions-page/insertions-page.component";
import {RequestsPageComponent} from "./request/requests-page/requests-page.component";
import {RequestDetailComponent} from "./request/request-detail/request-detail.component";
import {ChatLayoutComponent} from "./chat/chat-layout/chat-layout.component";
import {UserChatComponent} from "./chat/user-chat/user-chat.component";
import {ViewAReviewComponent} from "./review/view-areview/view-areview.component";
import {ShopWindowComponent} from "./insertions/shopWindow/shop-window.component";




const routes: Routes = [
  { path: 'home', component: HomeComponent},
  { path: 'insertions/vetrina/:userId', component: ShopWindowComponent},
  { path: 'insertions/:insertionId', component: InsertionDetailsComponent},
  { path: 'insertions/profile/:userId', component: InsertionsByIdComponent},
  { path: 'insertions', component: FormInsertionsComponent},
  { path: 'insertions/viewInsertions/:argument', component: InsertionsPageComponent},
  { path: 'requests/viewRequests/menu/:argument', component: RequestsPageComponent},
  { path: 'requests', component: CheckPageRequestComponent},
  { path: 'requests/viewActiveRequests', component: RequestComponent},
  { path: 'requests/insertNewRequest', component: CreateRequestComponent},
  { path: 'requests/viewRequests/:userId', component: RequestByIdComponent},
  { path: 'requests/:idRequest', component: RequestDetailComponent},
  { path: 'newlogin', component: FacebookLogoutComponent},
  { path: 'login', component: FacebookLoginComponent},
  { path: 'addInsertion', component: CreateInsertionComponent},
  { path: 'uploadImage/:insertionId', component: UploadImageComponent},
  { path: 'users/profile/:userId', component: ProfiloComponent},
  { path: 'purchases/:userId', component: ViewPurchaseComponent},
  { path: 'purchases/addReview/:insertionId', component: AddReviewComponent},
  { path: 'reviews/viewReview/:userId', component: ViewReviewComponent},
  { path: 'reviews/viewAReview/:reviewId', component: ViewAReviewComponent},
  { path: 'responses/addReviewResponse/:reviewId', component: AddReviewReponseComponent},
  { path: 'chat/sendMessage/:idReceiver', component: ChatLayoutComponent},
  { path: 'chat/viewChats/:userId', component: ChatLayoutComponent},
  { path: 'chat/viewChats/usersChat/:user1', component: UsersChatComponent},
  { path: 'login/confirmed', component: FacebookLogoutComponent},
  {
    path: 'dashboard',
    component: DashboardComponent,

  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
