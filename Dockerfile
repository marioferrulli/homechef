FROM openjdk:8
ADD target/spring-server.jar spring-server.jar
EXPOSE 8080
COPY wait-for-it.sh wait-for-it.sh
RUN mkdir myStorage
COPY src/main/resources/img/insertion101 myStorage/insertion101
COPY src/main/resources/img/insertion102 myStorage/insertion102
COPY src/main/resources/img/insertion103 myStorage/insertion103
RUN chmod +x wait-for-it.sh